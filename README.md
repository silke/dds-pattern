# Design of Digital Systems Assignments
This repository contains the two assignments for the course *Design of Digital Systems* (192130022).

The project is automatically tested with [`ghdl`][ghdl].

[ghdl]: http://ghdl.free.fr/
