\section{Implementation}

\subsection{Structural description}
A structural description is created consisting of several components:
a counter, display driver, pattern detector.
Furthermore a component is created to attach all subcomponents together.
A schematic overview of the design is shown in figure~\ref{fig:strct:overview}.

\begin{figure}
    \includegraphics{pattern_strct}
    \caption{Structural design of the pattern recognizer.}
    \label{fig:strct:overview}
\end{figure}

\subsubsection{Pattern detector}
The pattern detector in \texttt{list\_det.vhd} is almost the same as the one in the behavioural description:

\vlisting{11}{19}{list_det.vhd}

As in the behavioural description, the data bit is shifted into a register.
The major difference is that the register is only four bits in size:
the last bit is compared directly from the data input.
This is done to avoid a single cycle delay.

\subsubsection{Counter}
The implementation of the counter in \texttt{counter.vhd} is fairly similar to the behavioural description:

\vlisting{11}{37}{counter.vhd}

The decimals are again separated, but each limited to a maximum of 10 (10 denoting overflow).

The counters count when there is a match and there is no overflow.
If both counters are 9 (the number 99),
\texttt{max} is set to \texttt{true} and both counters to 10.
Otherwise they count in a regular fashion.
The output signals \texttt{bcd1} and \texttt{bcd2} are set to the value of the counters.

\subsubsection{Display driver}
The display driver is in \texttt{disp\_drv.vhd} is almost the same as the case statement from the behavioural description:

\vlisting{10}{25}{disp_drv.vhd}

\subsubsection{Structure}
The components are connected to each other in \texttt{pattern\_struct.vhd},
which provides the same functionality as the behavioural description in \texttt{pattern\_recognizer.vhd}.


\subsection{Simulation}
The test environment in \texttt{test\_env\_golden\_unit.vhdl} simulates the structural description and compares it against the behavioural description via \texttt{compare.vhdl}:

\vlisting{11}{20}{compare.vhdl}

The variable \texttt{r} denotes if there has ever been a reset.
It is set to \texttt{false} by default.
On the first reset this variable is set to \texttt{true}.
Assertions check if the segment displays show the same value on the falling edge of the clock when \texttt{r} is true.
The falling edge of the clock is used because these values should be the same only after a slight delay.

\begin{figure*}
    \includegraphics{sim_behvsstr_cnt}
    \margincaption{Comparison between a detection and subsequent increase in count between the behavioural and structural model.\label{fig:sim:compare:cnt}}
\end{figure*}

Simulation shows that the structural description functions the same as the behavioural description.
Figure~\ref{fig:sim:compare:cnt} shows the behaviour around the detection of a pattern where the count is increased correctly.
Figure~\ref{fig:sim:compare:rst} shows the behaviour around a reset where the count is reset to zero and starts detecting and counting again.

\begin{figure*}
    \includegraphics{sim_behvsstr_rst}
    \margincaption{Comparison between the behaviour around a reset between the behavioural and structural model.\label{fig:sim:compare:rst}}
\end{figure*}

\subsection{Synthesis}

Quartus is used for synthesis of the structural model.
The RTL view of the pattern recogniser is shown in figure~\ref{fig:syn}.
Only one of the two display drivers is shown in the figure.

13 registers are used in total.
Four are used for the shift register, containing the previous values of the data line.
There are four registers used for every BCD value of the counter.
One register is used to store whether overflow has occurred.

\begin{margintable}
    \centering
    \caption{Output timing of the seven segment displays.}
    \label{tbl:sim:delay}
    \begin{tabular}{r r}
        \toprule
        Segment & Time (ns) \\
        \midrule
        1 & 8.395 \\
        2 & 8.057 \\
        \bottomrule
    \end{tabular}
\end{margintable}

The timing analysis of Quartus shows that all signals on the seven segment displays are valid after 8.4 ns.
Table~\ref{tbl:sim:delay} shows the clock to output timings of the pattern recogniser.

Figure~\ref{fig:sim:delay} shows that the delay in the implementation is lower than half a clock cycle.
From the The cursor difference, showing the difference between the clock edge and the stabilisation of the display driver output, is 8.27 ns.
This matches the delay given by the Quartus TimeQuest analysis.

\begin{figure*}
    \includegraphics{pattern_sim_delay}
    \margincaption{Delay between behavioural and structural model in simulation.\label{fig:sim:delay}}
\end{figure*}

\begin{figure*}
    \includegraphics{pattern_rtl}
    \margincaption{Synthesised model of the pattern detector with only one of the display drivers expanded.\label{fig:syn}}
\end{figure*}
