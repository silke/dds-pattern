-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 32-bit"
-- VERSION "Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"

-- DATE "03/02/2017 11:02:50"

-- 
-- Device: Altera EP2C20F484C7 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEII;
LIBRARY IEEE;
USE CYCLONEII.CYCLONEII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	pattern_struct IS
    PORT (
	data : IN std_logic;
	reset : IN std_logic;
	clk : IN std_logic;
	seg1 : OUT std_logic_vector(6 DOWNTO 0);
	seg2 : OUT std_logic_vector(6 DOWNTO 0)
	);
END pattern_struct;

-- Design Ports Information
-- seg1[0]	=>  Location: PIN_A16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- seg1[1]	=>  Location: PIN_B15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- seg1[2]	=>  Location: PIN_A14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- seg1[3]	=>  Location: PIN_A15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- seg1[4]	=>  Location: PIN_B14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- seg1[5]	=>  Location: PIN_H12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- seg1[6]	=>  Location: PIN_C13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- seg2[0]	=>  Location: PIN_F12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- seg2[1]	=>  Location: PIN_F13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- seg2[2]	=>  Location: PIN_A17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- seg2[3]	=>  Location: PIN_E14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- seg2[4]	=>  Location: PIN_F14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- seg2[5]	=>  Location: PIN_B16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- seg2[6]	=>  Location: PIN_D14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- clk	=>  Location: PIN_M1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- reset	=>  Location: PIN_M2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- data	=>  Location: PIN_G12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default


ARCHITECTURE structure OF pattern_struct IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_data : std_logic;
SIGNAL ww_reset : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_seg1 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_seg2 : std_logic_vector(6 DOWNTO 0);
SIGNAL \clk~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \reset~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clk~combout\ : std_logic;
SIGNAL \clk~clkctrl_outclk\ : std_logic;
SIGNAL \cnt|cnt1[0]~4_combout\ : std_logic;
SIGNAL \reset~combout\ : std_logic;
SIGNAL \reset~clkctrl_outclk\ : std_logic;
SIGNAL \data~combout\ : std_logic;
SIGNAL \cnt|cnt2[2]~2_combout\ : std_logic;
SIGNAL \cnt|cnt2[2]~3_combout\ : std_logic;
SIGNAL \cnt|cnt2[2]~4_combout\ : std_logic;
SIGNAL \cnt|cnt2[3]~5_combout\ : std_logic;
SIGNAL \cnt|cnt2[0]~0_combout\ : std_logic;
SIGNAL \cnt|cnt2[1]~1_combout\ : std_logic;
SIGNAL \cnt|max~0_combout\ : std_logic;
SIGNAL \cnt|max~1_combout\ : std_logic;
SIGNAL \cnt|max~regout\ : std_logic;
SIGNAL \cnt|process_0~0_combout\ : std_logic;
SIGNAL \cnt|process_0~1_combout\ : std_logic;
SIGNAL \cnt|cnt1~2_combout\ : std_logic;
SIGNAL \cnt|cnt1~3_combout\ : std_logic;
SIGNAL \cnt|Equal0~0_combout\ : std_logic;
SIGNAL \cnt|cnt1~0_combout\ : std_logic;
SIGNAL \cnt|cnt1[2]~1_combout\ : std_logic;
SIGNAL \disp_drv1|Mux6~0_combout\ : std_logic;
SIGNAL \disp_drv1|Mux5~0_combout\ : std_logic;
SIGNAL \disp_drv1|Mux4~0_combout\ : std_logic;
SIGNAL \disp_drv1|Mux3~0_combout\ : std_logic;
SIGNAL \disp_drv1|Mux2~0_combout\ : std_logic;
SIGNAL \disp_drv1|Mux1~0_combout\ : std_logic;
SIGNAL \disp_drv1|Mux0~0_combout\ : std_logic;
SIGNAL \disp_drv2|Mux6~0_combout\ : std_logic;
SIGNAL \disp_drv2|Mux5~0_combout\ : std_logic;
SIGNAL \disp_drv2|Mux4~0_combout\ : std_logic;
SIGNAL \disp_drv2|Mux3~0_combout\ : std_logic;
SIGNAL \disp_drv2|Mux2~0_combout\ : std_logic;
SIGNAL \disp_drv2|Mux1~0_combout\ : std_logic;
SIGNAL \disp_drv2|Mux0~0_combout\ : std_logic;
SIGNAL \ldet|reg\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \cnt|cnt1\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \cnt|cnt2\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \ALT_INV_reset~clkctrl_outclk\ : std_logic;
SIGNAL \disp_drv2|ALT_INV_Mux6~0_combout\ : std_logic;
SIGNAL \disp_drv1|ALT_INV_Mux6~0_combout\ : std_logic;

BEGIN

ww_data <= data;
ww_reset <= reset;
ww_clk <= clk;
seg1 <= ww_seg1;
seg2 <= ww_seg2;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\clk~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \clk~combout\);

\reset~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \reset~combout\);
\ALT_INV_reset~clkctrl_outclk\ <= NOT \reset~clkctrl_outclk\;
\disp_drv2|ALT_INV_Mux6~0_combout\ <= NOT \disp_drv2|Mux6~0_combout\;
\disp_drv1|ALT_INV_Mux6~0_combout\ <= NOT \disp_drv1|Mux6~0_combout\;

-- Location: PIN_M1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\clk~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_clk,
	combout => \clk~combout\);

-- Location: CLKCTRL_G3
\clk~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~clkctrl_outclk\);

-- Location: LCCOMB_X34_Y26_N28
\cnt|cnt1[0]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \cnt|cnt1[0]~4_combout\ = !\cnt|cnt1\(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \cnt|cnt1\(0),
	combout => \cnt|cnt1[0]~4_combout\);

-- Location: PIN_M2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\reset~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_reset,
	combout => \reset~combout\);

-- Location: CLKCTRL_G1
\reset~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \reset~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \reset~clkctrl_outclk\);

-- Location: PIN_G12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\data~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_data,
	combout => \data~combout\);

-- Location: LCFF_X34_Y26_N11
\ldet|reg[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \data~combout\,
	aclr => \ALT_INV_reset~clkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ldet|reg\(0));

-- Location: LCFF_X34_Y26_N9
\ldet|reg[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \ldet|reg\(0),
	aclr => \ALT_INV_reset~clkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ldet|reg\(1));

-- Location: LCCOMB_X34_Y26_N10
\cnt|cnt2[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \cnt|cnt2[2]~2_combout\ = (\cnt|cnt2\(1) & (!\ldet|reg\(0) & \ldet|reg\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt2\(1),
	datac => \ldet|reg\(0),
	datad => \ldet|reg\(1),
	combout => \cnt|cnt2[2]~2_combout\);

-- Location: LCCOMB_X34_Y26_N20
\cnt|cnt2[2]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \cnt|cnt2[2]~3_combout\ = (\cnt|cnt2\(0) & (\cnt|process_0~0_combout\ & (\cnt|cnt2[2]~2_combout\ & \cnt|Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt2\(0),
	datab => \cnt|process_0~0_combout\,
	datac => \cnt|cnt2[2]~2_combout\,
	datad => \cnt|Equal0~0_combout\,
	combout => \cnt|cnt2[2]~3_combout\);

-- Location: LCCOMB_X34_Y26_N14
\cnt|cnt2[2]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \cnt|cnt2[2]~4_combout\ = \cnt|cnt2\(2) $ (\cnt|cnt2[2]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \cnt|cnt2\(2),
	datad => \cnt|cnt2[2]~3_combout\,
	combout => \cnt|cnt2[2]~4_combout\);

-- Location: LCFF_X34_Y26_N15
\cnt|cnt2[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \cnt|cnt2[2]~4_combout\,
	aclr => \ALT_INV_reset~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \cnt|cnt2\(2));

-- Location: LCCOMB_X34_Y26_N22
\cnt|cnt2[3]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \cnt|cnt2[3]~5_combout\ = \cnt|cnt2\(3) $ (((\cnt|cnt2\(2) & \cnt|cnt2[2]~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \cnt|cnt2\(2),
	datac => \cnt|cnt2\(3),
	datad => \cnt|cnt2[2]~3_combout\,
	combout => \cnt|cnt2[3]~5_combout\);

-- Location: LCFF_X34_Y26_N23
\cnt|cnt2[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \cnt|cnt2[3]~5_combout\,
	aclr => \ALT_INV_reset~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \cnt|cnt2\(3));

-- Location: LCCOMB_X34_Y26_N6
\cnt|cnt2[0]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \cnt|cnt2[0]~0_combout\ = \cnt|cnt2\(0) $ (((\cnt|Equal0~0_combout\ & \cnt|process_0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \cnt|Equal0~0_combout\,
	datac => \cnt|cnt2\(0),
	datad => \cnt|process_0~1_combout\,
	combout => \cnt|cnt2[0]~0_combout\);

-- Location: LCFF_X34_Y26_N7
\cnt|cnt2[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \cnt|cnt2[0]~0_combout\,
	aclr => \ALT_INV_reset~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \cnt|cnt2\(0));

-- Location: LCCOMB_X34_Y26_N12
\cnt|cnt2[1]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \cnt|cnt2[1]~1_combout\ = \cnt|cnt2\(1) $ (((\cnt|cnt2\(0) & (\cnt|Equal0~0_combout\ & \cnt|process_0~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt2\(0),
	datab => \cnt|Equal0~0_combout\,
	datac => \cnt|cnt2\(1),
	datad => \cnt|process_0~1_combout\,
	combout => \cnt|cnt2[1]~1_combout\);

-- Location: LCFF_X34_Y26_N13
\cnt|cnt2[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \cnt|cnt2[1]~1_combout\,
	aclr => \ALT_INV_reset~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \cnt|cnt2\(1));

-- Location: LCCOMB_X35_Y26_N30
\cnt|max~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \cnt|max~0_combout\ = (!\cnt|cnt2\(2) & (\cnt|cnt2\(3) & (\cnt|cnt2\(0) & !\cnt|cnt2\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt2\(2),
	datab => \cnt|cnt2\(3),
	datac => \cnt|cnt2\(0),
	datad => \cnt|cnt2\(1),
	combout => \cnt|max~0_combout\);

-- Location: LCCOMB_X34_Y26_N2
\cnt|max~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \cnt|max~1_combout\ = (\cnt|max~regout\) # ((\cnt|Equal0~0_combout\ & (\cnt|process_0~1_combout\ & \cnt|max~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|Equal0~0_combout\,
	datab => \cnt|process_0~1_combout\,
	datac => \cnt|max~regout\,
	datad => \cnt|max~0_combout\,
	combout => \cnt|max~1_combout\);

-- Location: LCFF_X34_Y26_N3
\cnt|max\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \cnt|max~1_combout\,
	aclr => \ALT_INV_reset~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \cnt|max~regout\);

-- Location: LCFF_X34_Y26_N29
\ldet|reg[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \ldet|reg\(1),
	aclr => \ALT_INV_reset~clkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ldet|reg\(2));

-- Location: LCFF_X34_Y26_N19
\ldet|reg[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \ldet|reg\(2),
	aclr => \ALT_INV_reset~clkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ldet|reg\(3));

-- Location: LCCOMB_X34_Y26_N18
\cnt|process_0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \cnt|process_0~0_combout\ = (!\data~combout\ & (!\cnt|max~regout\ & (\ldet|reg\(3) & \ldet|reg\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \data~combout\,
	datab => \cnt|max~regout\,
	datac => \ldet|reg\(3),
	datad => \ldet|reg\(2),
	combout => \cnt|process_0~0_combout\);

-- Location: LCCOMB_X34_Y26_N8
\cnt|process_0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \cnt|process_0~1_combout\ = (!\ldet|reg\(0) & (\ldet|reg\(1) & \cnt|process_0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ldet|reg\(0),
	datac => \ldet|reg\(1),
	datad => \cnt|process_0~0_combout\,
	combout => \cnt|process_0~1_combout\);

-- Location: LCFF_X34_Y26_N1
\cnt|cnt1[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \cnt|cnt1[0]~4_combout\,
	aclr => \ALT_INV_reset~clkctrl_outclk\,
	sload => VCC,
	ena => \cnt|process_0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \cnt|cnt1\(0));

-- Location: LCCOMB_X34_Y26_N24
\cnt|cnt1~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \cnt|cnt1~2_combout\ = (\cnt|cnt1\(2) & (\cnt|cnt1\(3) $ (((\cnt|cnt1\(1) & \cnt|cnt1\(0)))))) # (!\cnt|cnt1\(2) & (\cnt|cnt1\(3) & ((\cnt|cnt1\(1)) # (!\cnt|cnt1\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt1\(2),
	datab => \cnt|cnt1\(1),
	datac => \cnt|cnt1\(3),
	datad => \cnt|cnt1\(0),
	combout => \cnt|cnt1~2_combout\);

-- Location: LCCOMB_X34_Y26_N4
\cnt|cnt1~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \cnt|cnt1~3_combout\ = (\cnt|cnt1~2_combout\) # ((\cnt|Equal0~0_combout\ & \cnt|max~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \cnt|Equal0~0_combout\,
	datac => \cnt|cnt1~2_combout\,
	datad => \cnt|max~0_combout\,
	combout => \cnt|cnt1~3_combout\);

-- Location: LCFF_X34_Y26_N5
\cnt|cnt1[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \cnt|cnt1~3_combout\,
	aclr => \ALT_INV_reset~clkctrl_outclk\,
	ena => \cnt|process_0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \cnt|cnt1\(3));

-- Location: LCCOMB_X34_Y26_N26
\cnt|Equal0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \cnt|Equal0~0_combout\ = (!\cnt|cnt1\(2) & (!\cnt|cnt1\(1) & (\cnt|cnt1\(3) & \cnt|cnt1\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt1\(2),
	datab => \cnt|cnt1\(1),
	datac => \cnt|cnt1\(3),
	datad => \cnt|cnt1\(0),
	combout => \cnt|Equal0~0_combout\);

-- Location: LCCOMB_X34_Y26_N30
\cnt|cnt1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \cnt|cnt1~0_combout\ = (\cnt|Equal0~0_combout\ & (((\cnt|max~0_combout\)))) # (!\cnt|Equal0~0_combout\ & (\cnt|cnt1\(0) $ ((\cnt|cnt1\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111000010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt1\(0),
	datab => \cnt|Equal0~0_combout\,
	datac => \cnt|cnt1\(1),
	datad => \cnt|max~0_combout\,
	combout => \cnt|cnt1~0_combout\);

-- Location: LCFF_X34_Y26_N31
\cnt|cnt1[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \cnt|cnt1~0_combout\,
	aclr => \ALT_INV_reset~clkctrl_outclk\,
	ena => \cnt|process_0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \cnt|cnt1\(1));

-- Location: LCCOMB_X34_Y26_N16
\cnt|cnt1[2]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \cnt|cnt1[2]~1_combout\ = \cnt|cnt1\(2) $ (((\cnt|cnt1\(0) & (\cnt|cnt1\(1) & \cnt|process_0~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt1\(0),
	datab => \cnt|cnt1\(1),
	datac => \cnt|cnt1\(2),
	datad => \cnt|process_0~1_combout\,
	combout => \cnt|cnt1[2]~1_combout\);

-- Location: LCFF_X34_Y26_N17
\cnt|cnt1[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \cnt|cnt1[2]~1_combout\,
	aclr => \ALT_INV_reset~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \cnt|cnt1\(2));

-- Location: LCCOMB_X34_Y26_N0
\disp_drv1|Mux6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \disp_drv1|Mux6~0_combout\ = (\cnt|cnt1\(3)) # (\cnt|cnt1\(1) $ (((\cnt|cnt1\(2) & \cnt|cnt1\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111111101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt1\(2),
	datab => \cnt|cnt1\(3),
	datac => \cnt|cnt1\(0),
	datad => \cnt|cnt1\(1),
	combout => \disp_drv1|Mux6~0_combout\);

-- Location: LCCOMB_X33_Y26_N20
\disp_drv1|Mux5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \disp_drv1|Mux5~0_combout\ = (\cnt|cnt1\(0) & ((\cnt|cnt1\(1)) # (\cnt|cnt1\(2) $ (!\cnt|cnt1\(3))))) # (!\cnt|cnt1\(0) & ((\cnt|cnt1\(2) & ((\cnt|cnt1\(3)))) # (!\cnt|cnt1\(2) & (\cnt|cnt1\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt1\(1),
	datab => \cnt|cnt1\(0),
	datac => \cnt|cnt1\(2),
	datad => \cnt|cnt1\(3),
	combout => \disp_drv1|Mux5~0_combout\);

-- Location: LCCOMB_X33_Y26_N10
\disp_drv1|Mux4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \disp_drv1|Mux4~0_combout\ = (\cnt|cnt1\(0)) # ((\cnt|cnt1\(3) & ((\cnt|cnt1\(1)) # (\cnt|cnt1\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt1\(1),
	datab => \cnt|cnt1\(0),
	datac => \cnt|cnt1\(2),
	datad => \cnt|cnt1\(3),
	combout => \disp_drv1|Mux4~0_combout\);

-- Location: LCCOMB_X33_Y26_N0
\disp_drv1|Mux3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \disp_drv1|Mux3~0_combout\ = (\cnt|cnt1\(1) & ((\cnt|cnt1\(3)) # ((\cnt|cnt1\(0) & \cnt|cnt1\(2))))) # (!\cnt|cnt1\(1) & (\cnt|cnt1\(2) $ (((\cnt|cnt1\(0) & !\cnt|cnt1\(3))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt1\(1),
	datab => \cnt|cnt1\(0),
	datac => \cnt|cnt1\(2),
	datad => \cnt|cnt1\(3),
	combout => \disp_drv1|Mux3~0_combout\);

-- Location: LCCOMB_X33_Y26_N6
\disp_drv1|Mux2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \disp_drv1|Mux2~0_combout\ = (\cnt|cnt1\(3) & ((\cnt|cnt1\(1)) # ((\cnt|cnt1\(2))))) # (!\cnt|cnt1\(3) & (!\cnt|cnt1\(0) & (\cnt|cnt1\(1) $ (\cnt|cnt1\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt1\(1),
	datab => \cnt|cnt1\(0),
	datac => \cnt|cnt1\(2),
	datad => \cnt|cnt1\(3),
	combout => \disp_drv1|Mux2~0_combout\);

-- Location: LCCOMB_X33_Y26_N12
\disp_drv1|Mux1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \disp_drv1|Mux1~0_combout\ = (\cnt|cnt1\(2) & ((\cnt|cnt1\(3)) # (\cnt|cnt1\(1) $ (\cnt|cnt1\(0))))) # (!\cnt|cnt1\(2) & (\cnt|cnt1\(1) & ((\cnt|cnt1\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt1\(1),
	datab => \cnt|cnt1\(0),
	datac => \cnt|cnt1\(2),
	datad => \cnt|cnt1\(3),
	combout => \disp_drv1|Mux1~0_combout\);

-- Location: LCCOMB_X33_Y26_N22
\disp_drv1|Mux0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \disp_drv1|Mux0~0_combout\ = (\cnt|cnt1\(1) & (((\cnt|cnt1\(3))))) # (!\cnt|cnt1\(1) & (\cnt|cnt1\(2) $ (((\cnt|cnt1\(0) & !\cnt|cnt1\(3))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt1\(1),
	datab => \cnt|cnt1\(0),
	datac => \cnt|cnt1\(2),
	datad => \cnt|cnt1\(3),
	combout => \disp_drv1|Mux0~0_combout\);

-- Location: LCCOMB_X35_Y26_N20
\disp_drv2|Mux6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \disp_drv2|Mux6~0_combout\ = (\cnt|cnt2\(3)) # (\cnt|cnt2\(1) $ (((\cnt|cnt2\(2) & \cnt|cnt2\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111111101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt2\(2),
	datab => \cnt|cnt2\(3),
	datac => \cnt|cnt2\(0),
	datad => \cnt|cnt2\(1),
	combout => \disp_drv2|Mux6~0_combout\);

-- Location: LCCOMB_X35_Y26_N2
\disp_drv2|Mux5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \disp_drv2|Mux5~0_combout\ = (\cnt|cnt2\(2) & ((\cnt|cnt2\(3)) # ((\cnt|cnt2\(0) & \cnt|cnt2\(1))))) # (!\cnt|cnt2\(2) & ((\cnt|cnt2\(1)) # ((!\cnt|cnt2\(3) & \cnt|cnt2\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110110011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt2\(2),
	datab => \cnt|cnt2\(3),
	datac => \cnt|cnt2\(0),
	datad => \cnt|cnt2\(1),
	combout => \disp_drv2|Mux5~0_combout\);

-- Location: LCCOMB_X35_Y26_N12
\disp_drv2|Mux4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \disp_drv2|Mux4~0_combout\ = (\cnt|cnt2\(0)) # ((\cnt|cnt2\(3) & ((\cnt|cnt2\(2)) # (\cnt|cnt2\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt2\(2),
	datab => \cnt|cnt2\(3),
	datac => \cnt|cnt2\(0),
	datad => \cnt|cnt2\(1),
	combout => \disp_drv2|Mux4~0_combout\);

-- Location: LCCOMB_X35_Y26_N22
\disp_drv2|Mux3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \disp_drv2|Mux3~0_combout\ = (\cnt|cnt2\(1) & ((\cnt|cnt2\(3)) # ((\cnt|cnt2\(2) & \cnt|cnt2\(0))))) # (!\cnt|cnt2\(1) & (\cnt|cnt2\(2) $ (((!\cnt|cnt2\(3) & \cnt|cnt2\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt2\(2),
	datab => \cnt|cnt2\(3),
	datac => \cnt|cnt2\(0),
	datad => \cnt|cnt2\(1),
	combout => \disp_drv2|Mux3~0_combout\);

-- Location: LCCOMB_X35_Y26_N0
\disp_drv2|Mux2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \disp_drv2|Mux2~0_combout\ = (\cnt|cnt2\(3) & ((\cnt|cnt2\(2)) # ((\cnt|cnt2\(1))))) # (!\cnt|cnt2\(3) & (!\cnt|cnt2\(0) & (\cnt|cnt2\(2) $ (\cnt|cnt2\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110110001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt2\(2),
	datab => \cnt|cnt2\(3),
	datac => \cnt|cnt2\(0),
	datad => \cnt|cnt2\(1),
	combout => \disp_drv2|Mux2~0_combout\);

-- Location: LCCOMB_X35_Y26_N26
\disp_drv2|Mux1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \disp_drv2|Mux1~0_combout\ = (\cnt|cnt2\(2) & ((\cnt|cnt2\(3)) # (\cnt|cnt2\(0) $ (\cnt|cnt2\(1))))) # (!\cnt|cnt2\(2) & (\cnt|cnt2\(3) & ((\cnt|cnt2\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt2\(2),
	datab => \cnt|cnt2\(3),
	datac => \cnt|cnt2\(0),
	datad => \cnt|cnt2\(1),
	combout => \disp_drv2|Mux1~0_combout\);

-- Location: LCCOMB_X35_Y26_N28
\disp_drv2|Mux0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \disp_drv2|Mux0~0_combout\ = (\cnt|cnt2\(1) & (((\cnt|cnt2\(3))))) # (!\cnt|cnt2\(1) & (\cnt|cnt2\(2) $ (((!\cnt|cnt2\(3) & \cnt|cnt2\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \cnt|cnt2\(2),
	datab => \cnt|cnt2\(3),
	datac => \cnt|cnt2\(0),
	datad => \cnt|cnt2\(1),
	combout => \disp_drv2|Mux0~0_combout\);

-- Location: PIN_A16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\seg1[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \disp_drv1|ALT_INV_Mux6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_seg1(0));

-- Location: PIN_B15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\seg1[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \disp_drv1|Mux5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_seg1(1));

-- Location: PIN_A14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\seg1[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \disp_drv1|Mux4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_seg1(2));

-- Location: PIN_A15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\seg1[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \disp_drv1|Mux3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_seg1(3));

-- Location: PIN_B14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\seg1[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \disp_drv1|Mux2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_seg1(4));

-- Location: PIN_H12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\seg1[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \disp_drv1|Mux1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_seg1(5));

-- Location: PIN_C13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\seg1[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \disp_drv1|Mux0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_seg1(6));

-- Location: PIN_F12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\seg2[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \disp_drv2|ALT_INV_Mux6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_seg2(0));

-- Location: PIN_F13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\seg2[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \disp_drv2|Mux5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_seg2(1));

-- Location: PIN_A17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\seg2[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \disp_drv2|Mux4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_seg2(2));

-- Location: PIN_E14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\seg2[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \disp_drv2|Mux3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_seg2(3));

-- Location: PIN_F14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\seg2[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \disp_drv2|Mux2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_seg2(4));

-- Location: PIN_B16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\seg2[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \disp_drv2|Mux1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_seg2(5));

-- Location: PIN_D14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\seg2[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \disp_drv2|Mux0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_seg2(6));
END structure;


