library ieee;
use ieee.std_logic_1164.all;
entity compare is
  port (seg_beh1, seg_beh2, seg_str1, seg_str2 : in std_logic_vector(6 downto 0);
        reset                                  : in std_logic;
        clk                                    : in std_logic);
end compare;

architecture comparison of compare is
begin
  process(reset, clk)
    variable r : boolean := false;
  begin
    if reset = '0' then
      r := true;
    elsif falling_edge(clk) and r then
      assert seg_beh1 = seg_str1 report "Seg1 behaviour differs from design step" severity warning;
      assert seg_beh2 = seg_str2 report "Seg2 behaviour differs from design step" severity warning;
    end if;
  end process;
end comparison;
