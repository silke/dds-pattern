library ieee;
use ieee.std_logic_1164.all;
entity counter is
  port(rst, clk    : in  std_logic;
        match      : in  boolean;
        bcd1, bcd2 : out integer range 0 to 10);
end counter;

architecture synth of counter is
begin
  process(rst, clk)
    variable cnt1, cnt2 : integer range 0 to 10;
    variable max        : boolean;
  begin
    if rst = '0' then
      cnt1 := 0;
      cnt2 := 0;
      max  := false;
    elsif rising_edge(clk) then
      if (max = false) and (match = true) then
        if cnt1 = 9 then
          if cnt2 = 9 then
            cnt1 := 10;
            cnt2 := 10;
            max  := true;
          else
            cnt1 := 0;
            cnt2 := cnt2 + 1;
          end if;
        else
          cnt1 := cnt1 + 1;
        end if;
      end if;
    end if;
    bcd1 <= cnt1;
    bcd2 <= cnt2;
  end process;
end architecture;

