library ieee;
use ieee.std_logic_1164.all;
entity disp_drv is
  port(bcd   : in  integer range 0 to 10;
        segm : out std_logic_vector(6 downto 0));
end disp_drv;

architecture synth of disp_drv is
begin
  process(bcd)
  begin
    case bcd is
      when 0      => segm <= "1000000";
      when 1      => segm <= "1111001";
      when 2      => segm <= "0100100";
      when 3      => segm <= "0110000";
      when 4      => segm <= "0011001";
      when 5      => segm <= "0010010";
      when 6      => segm <= "0000010";
      when 7      => segm <= "1111000";
      when 8      => segm <= "0000000";
      when 9      => segm <= "0010000";
      when others => segm <= "0111111";
    end case;
  end process;
end architecture;
