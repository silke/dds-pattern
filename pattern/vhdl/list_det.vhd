library ieee;
use ieee.std_logic_1164.all;
entity list_det is
  port(rst, clk, data : in  std_logic;
        match         : out boolean);
end list_det;

architecture synth of list_det is
  signal reg : std_logic_vector(3 downto 0);
begin
  process(rst, clk)
  begin
    if rst = '0' then
      reg   <= "0000";
    elsif rising_edge(clk) then
      reg <= reg(2 downto 0) & data;
    end if;
  end process;
  match <= true when (reg = "1110") and (data = '0') else false;  
end architecture;
