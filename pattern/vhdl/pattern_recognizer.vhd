library ieee;
use ieee.std_logic_1164.all;
entity pattern_recognizer is
  port (data  : in  std_logic;
        reset : in  std_logic;
        clk   : in  std_logic;
        seg1  : out std_logic_vector(6 downto 0);
        seg2  : out std_logic_vector(6 downto 0));
end pattern_recognizer;


-- The informal description of this design is:
--   - It is a synchronous system, with an asynchronous reset
--   - The active edge of all flip-flops is the rising edge of the clock
--   - It recognises the pattern 11100 (from left to right)
--   - It counts the number of occurrences of this pattern
--   - If a pattern is received, then the counter is updated (see figure below) after the rising edge of the
--   - It uses two seven-segment displays.
--   - After more than 99 occurrences the displays show '--' (overflow).
--   - After a reset the counter is zero again, if a part of the pattern was already recognised it is ignored.
--   - Before the first reset the circuit has an undefined behaviour.

architecture behaviour of pattern_recognizer is
  signal cnt0 : integer;
  signal cnt1 : integer;
  signal max  : boolean;

  function int2segm(i : integer) return std_logic_vector is
  begin
    case i is
      when 0      => return "1000000";
      when 1      => return "1111001";
      when 2      => return "0100100";
      when 3      => return "0110000";
      when 4      => return "0011001";
      when 5      => return "0010010";
      when 6      => return "0000010";
      when 7      => return "1111000";
      when 8      => return "0000000";
      when 9      => return "0010000";
      when others => return "0111111";
    end case;
  end int2segm;

begin
  process(reset, clk)
    -- your declarations
    variable c0, c1  : integer;
    variable m       : boolean;
    variable reg     : std_logic_vector(4 downto 0);
    constant PATTERN : std_logic_vector(4 downto 0) := "11100";
  begin
    if reset = '0' then
      -- reset actions
      c0  := 0;
      c1  := 0;
      m   := false;
      reg := "00000";
    elsif rising_edge(clk) then
      -- Shift registry
      reg := reg(3 downto 0) & data;

      -- Only do things when not in overflow
      if max then
        c0 := 10;
        c1 := 10;
      else
        -- If pattern matches, increase count
        if reg = PATTERN then
          c0 := cnt0 + 1;
        end if;

        -- Check counters
        if c0 = 10 then
          c0 := 0;
          c1 := c1 + 1;
        end if;
        if c1 = 10 then
          m  := true;
          c0 := 10;
        end if;
      end if;
    end if;

    -- Update display & signals
    seg1 <= int2segm(c0);
    seg2 <= int2segm(c1);
    cnt0 <= c0;
    cnt1 <= c1;
    max  <= m;
  end process;
end behaviour;
