library ieee;
use ieee.std_logic_1164.all;
entity pattern_struct is
  port (data  : in  std_logic;
        reset : in  std_logic;
        clk   : in  std_logic;
        seg1  : out std_logic_vector(6 downto 0);
        seg2  : out std_logic_vector(6 downto 0));
end pattern_struct;


-- The informal description of this design is:
--   - It is a synchronous system, with an asynchronous reset
--   - The active edge of all flip-flops is the rising edge of the clock
--   - It recognises the pattern 11100 (from left to right)
--   - It counts the number of occurrences of this pattern
--   - If a pattern is received, then the counter is updated (see figure below) after the rising edge of the
--   - It uses two seven-segment displays.
--   - After more than 99 occurrences the displays show '--' (overflow).
--   - After a reset the counter is zero again, if a part of the pattern was already recognised it is ignored.
--   - Before the first reset the circuit has an undefined behaviour.

architecture behaviour of pattern_struct is
  component list_det is
    port(rst, clk, data : in  std_logic;
          match         : out boolean);
  end component;
  component counter is
    port(rst, clk    : in  std_logic;
          match      : in  boolean;
          bcd1, bcd2 : out integer range 0 to 10);
  end component;
  component disp_drv is
    port(bcd   : in  integer range 0 to 10;
          segm : out std_logic_vector(6 downto 0));
  end component;
  signal bcd1, bcd2 : integer range 0 to 10;
  signal match      : boolean;
begin
  disp_drv1 : disp_drv port map(bcd1, seg1);
  disp_drv2 : disp_drv port map(bcd2, seg2);
  cnt       : counter port map(reset, clk, match, bcd1, bcd2);
  ldet      : list_det port map(reset, clk, data, match);
end behaviour;
