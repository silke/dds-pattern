library ieee;
use ieee.std_logic_1164.all;
entity testbench_golden is
end testbench_golden;

architecture structure of testbench_golden is
  signal seg_beh1, seg_beh2, seg_str1, seg_str2 : std_logic_vector(6 downto 0);
  signal d, rst, c                              : std_logic;

  component pattern_recognizer is
    port (data  : in  std_logic;
          reset : in  std_logic;
          clk   : in  std_logic;
          seg1  : out std_logic_vector(6 downto 0);
          seg2  : out std_logic_vector(6 downto 0));
  end component;

  component pattern_struct is
    port (data  : in  std_logic;
          reset : in  std_logic;
          clk   : in  std_logic;
          seg1  : out std_logic_vector(6 downto 0);
          seg2  : out std_logic_vector(6 downto 0));
  end component;

  component testset is
    port (data, reset, clk : out std_logic);
  end component;

  component compare is
    port (seg_beh1, seg_beh2, seg_str1, seg_str2 : in std_logic_vector(6 downto 0);
          reset                                  : in std_logic;
          clk                                    : in std_logic);
  end component;
begin
  p   : pattern_recognizer port map(d, rst, c, seg_beh1, seg_beh2);
  str : pattern_struct port map(d, rst, c, seg_str1, seg_str2);
  ts  : testset port map(d, rst, c);
  cmp : compare port map(seg_beh1, seg_beh2, seg_str1, seg_str2, rst, c);
end structure;


