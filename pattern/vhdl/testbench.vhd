library ieee;
use ieee.std_logic_1164.all;
entity testbench is
  port (seg1, seg2 : out std_logic_vector(6 downto 0));
end testbench;

architecture structure of testbench is
  component pattern_recognizer is
    port (data  : in  std_logic;
          reset : in  std_logic;
          clk   : in  std_logic;
          seg1  : out std_logic_vector(6 downto 0);
          seg2  : out std_logic_vector(6 downto 0));
  end component;
  component testset is
    port (data, reset, clk : out std_logic);
  end component;
  signal d, rst, c : std_logic;
begin
  p  : pattern_recognizer port map(d, rst, c, seg1, seg2);
  ts : testset port map(d, rst, c);
end structure;


