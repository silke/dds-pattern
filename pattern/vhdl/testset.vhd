library ieee;
use ieee.std_logic_1164.all;
entity testset is
  port (data, reset, clk : out std_logic := '0');
end testset;

architecture TestSet1 of testset is
  signal clock : std_logic := '0';
begin
  clock <= not clock after 10 ns;
  clk   <= clock;
  process
  begin
    wait for 100 ns;
    reset <= '1';
    wait for 500 ns;
    assert false report "the input pattern is generated 110 times" severity note;
    for i in 1 to 110 loop
      data <= '0';
      wait for 100 ns;
      data <= '1';
      wait for 200 ns;
      data <= '0';
      wait for 40 ns;
    end loop;
    reset <= '0';
    wait for 200 ns;
    reset <= '1';
    assert false report "the input pattern is generated 50 times" severity note;
    wait for 200 ns;
    for i in 1 to 50 loop
      data <= '0';
      wait for 100 ns;
      data <= '1';
      wait for 200 ns;
      data <= '0';
      wait for 40 ns;
    end loop;
    wait;
  end process;
end TestSet1;
