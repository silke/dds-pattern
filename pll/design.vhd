library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity design is
  port (L1, L2    : out std_logic;
        locked    : out std_logic;
        clk, rstn : in  std_logic);
end design;

architecture structure of design is
  component divider50M is
    port (y         : out std_logic;
          clk, rstn : in  std_logic);
  end component divider50M;

  component PLL is
    port
      (
        areset : in  std_logic := '0';
        inclk0 : in  std_logic := '0';
        c0     : out std_logic;
        locked : out std_logic
        );
  end component PLL;

  signal clk_mul4 : std_logic;
  signal rst      : std_logic;
begin

  normal : divider50M port map (L1, clk, rstn);
  fast   : divider50M port map (L2, clk_mul4, rstn);

  rst <= not rstn;
  pl : PLL port map (rst, clk, clk_mul4, locked);


end architecture structure;


