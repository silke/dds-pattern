library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity divider50M is
  port (y         : out std_logic;
        clk, rstn : in  std_logic);
end divider50M;

architecture bhv of divider50M is
  signal   yi  : std_logic;
  constant max : integer := 50000000;
begin
  process(clk, rstn)
    variable cnt : integer range 0 to max-1;
  begin
    if rstn = '0' then
      cnt := 0;
      yi  <= '0';
    elsif rising_edge(clk) then
      if cnt < max-1 then
        cnt := cnt+1;
      else
        cnt := 0;
        yi  <= not yi;
      end if;
    end if;
  end process;

  y <= yi;

end architecture bhv;
