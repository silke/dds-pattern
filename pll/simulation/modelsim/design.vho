-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 32-bit"
-- VERSION "Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"

-- DATE "03/02/2017 12:24:36"

-- 
-- Device: Altera EP2C20F484C7 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEII;
LIBRARY IEEE;
USE CYCLONEII.CYCLONEII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	design IS
    PORT (
	L1 : OUT std_logic;
	L2 : OUT std_logic;
	locked : OUT std_logic;
	clk : IN std_logic;
	rstn : IN std_logic
	);
END design;

-- Design Ports Information
-- L1	=>  Location: PIN_R19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- L2	=>  Location: PIN_U19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- locked	=>  Location: PIN_R20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- clk	=>  Location: PIN_L1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- rstn	=>  Location: PIN_R22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default


ARCHITECTURE structure OF design IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_L1 : std_logic;
SIGNAL ww_L2 : std_logic;
SIGNAL ww_locked : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_rstn : std_logic;
SIGNAL \pl|altpll_component|pll_INCLK_bus\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \pl|altpll_component|pll_CLK_bus\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \pl|altpll_component|_clk0~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clk~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \pl|altpll_component|pll~CLK1\ : std_logic;
SIGNAL \pl|altpll_component|pll~CLK2\ : std_logic;
SIGNAL \normal|cnt[0]~26_combout\ : std_logic;
SIGNAL \normal|cnt[2]~30_combout\ : std_logic;
SIGNAL \normal|cnt[3]~32_combout\ : std_logic;
SIGNAL \normal|cnt[18]~62_combout\ : std_logic;
SIGNAL \normal|cnt[19]~64_combout\ : std_logic;
SIGNAL \normal|cnt[23]~72_combout\ : std_logic;
SIGNAL \fast|cnt[0]~26_combout\ : std_logic;
SIGNAL \fast|cnt[2]~30_combout\ : std_logic;
SIGNAL \fast|cnt[3]~32_combout\ : std_logic;
SIGNAL \fast|cnt[9]~44_combout\ : std_logic;
SIGNAL \fast|cnt[13]~52_combout\ : std_logic;
SIGNAL \fast|cnt[15]~56_combout\ : std_logic;
SIGNAL \fast|cnt[19]~64_combout\ : std_logic;
SIGNAL \fast|cnt[24]~75\ : std_logic;
SIGNAL \fast|cnt[25]~76_combout\ : std_logic;
SIGNAL \normal|LessThan0~2_combout\ : std_logic;
SIGNAL \normal|LessThan0~7_combout\ : std_logic;
SIGNAL \fast|LessThan0~2_combout\ : std_logic;
SIGNAL \fast|LessThan0~5_combout\ : std_logic;
SIGNAL \clk~combout\ : std_logic;
SIGNAL \clk~clkctrl_outclk\ : std_logic;
SIGNAL \normal|cnt[0]~27\ : std_logic;
SIGNAL \normal|cnt[1]~28_combout\ : std_logic;
SIGNAL \rstn~combout\ : std_logic;
SIGNAL \normal|cnt[9]~44_combout\ : std_logic;
SIGNAL \normal|LessThan0~1_combout\ : std_logic;
SIGNAL \normal|cnt[5]~36_combout\ : std_logic;
SIGNAL \normal|LessThan0~3_combout\ : std_logic;
SIGNAL \normal|cnt[14]~55\ : std_logic;
SIGNAL \normal|cnt[15]~56_combout\ : std_logic;
SIGNAL \normal|cnt[15]~57\ : std_logic;
SIGNAL \normal|cnt[16]~58_combout\ : std_logic;
SIGNAL \normal|cnt[7]~40_combout\ : std_logic;
SIGNAL \normal|cnt[16]~59\ : std_logic;
SIGNAL \normal|cnt[17]~60_combout\ : std_logic;
SIGNAL \normal|cnt[17]~61\ : std_logic;
SIGNAL \normal|cnt[18]~63\ : std_logic;
SIGNAL \normal|cnt[19]~65\ : std_logic;
SIGNAL \normal|cnt[20]~66_combout\ : std_logic;
SIGNAL \normal|cnt[20]~67\ : std_logic;
SIGNAL \normal|cnt[21]~69\ : std_logic;
SIGNAL \normal|cnt[22]~70_combout\ : std_logic;
SIGNAL \normal|cnt[22]~71\ : std_logic;
SIGNAL \normal|cnt[23]~73\ : std_logic;
SIGNAL \normal|cnt[24]~74_combout\ : std_logic;
SIGNAL \normal|LessThan0~0_combout\ : std_logic;
SIGNAL \normal|LessThan0~4_combout\ : std_logic;
SIGNAL \normal|cnt[24]~75\ : std_logic;
SIGNAL \normal|cnt[25]~76_combout\ : std_logic;
SIGNAL \normal|cnt[21]~68_combout\ : std_logic;
SIGNAL \normal|LessThan0~8_combout\ : std_logic;
SIGNAL \normal|LessThan0~9_combout\ : std_logic;
SIGNAL \normal|LessThan0~10_combout\ : std_logic;
SIGNAL \normal|cnt[1]~29\ : std_logic;
SIGNAL \normal|cnt[2]~31\ : std_logic;
SIGNAL \normal|cnt[3]~33\ : std_logic;
SIGNAL \normal|cnt[4]~34_combout\ : std_logic;
SIGNAL \normal|cnt[4]~35\ : std_logic;
SIGNAL \normal|cnt[5]~37\ : std_logic;
SIGNAL \normal|cnt[6]~38_combout\ : std_logic;
SIGNAL \normal|cnt[6]~39\ : std_logic;
SIGNAL \normal|cnt[7]~41\ : std_logic;
SIGNAL \normal|cnt[8]~42_combout\ : std_logic;
SIGNAL \normal|cnt[8]~43\ : std_logic;
SIGNAL \normal|cnt[9]~45\ : std_logic;
SIGNAL \normal|cnt[10]~46_combout\ : std_logic;
SIGNAL \normal|cnt[10]~47\ : std_logic;
SIGNAL \normal|cnt[11]~48_combout\ : std_logic;
SIGNAL \normal|cnt[11]~49\ : std_logic;
SIGNAL \normal|cnt[12]~50_combout\ : std_logic;
SIGNAL \normal|cnt[12]~51\ : std_logic;
SIGNAL \normal|cnt[13]~52_combout\ : std_logic;
SIGNAL \normal|cnt[13]~53\ : std_logic;
SIGNAL \normal|cnt[14]~54_combout\ : std_logic;
SIGNAL \normal|LessThan0~5_combout\ : std_logic;
SIGNAL \normal|LessThan0~6_combout\ : std_logic;
SIGNAL \normal|yi~0_combout\ : std_logic;
SIGNAL \normal|yi~regout\ : std_logic;
SIGNAL \pl|altpll_component|_clk0\ : std_logic;
SIGNAL \pl|altpll_component|_clk0~clkctrl_outclk\ : std_logic;
SIGNAL \fast|cnt[0]~27\ : std_logic;
SIGNAL \fast|cnt[1]~28_combout\ : std_logic;
SIGNAL \fast|cnt[8]~43\ : std_logic;
SIGNAL \fast|cnt[9]~45\ : std_logic;
SIGNAL \fast|cnt[10]~46_combout\ : std_logic;
SIGNAL \fast|cnt[10]~47\ : std_logic;
SIGNAL \fast|cnt[11]~48_combout\ : std_logic;
SIGNAL \fast|cnt[11]~49\ : std_logic;
SIGNAL \fast|cnt[12]~50_combout\ : std_logic;
SIGNAL \fast|cnt[12]~51\ : std_logic;
SIGNAL \fast|cnt[13]~53\ : std_logic;
SIGNAL \fast|cnt[14]~54_combout\ : std_logic;
SIGNAL \fast|cnt[14]~55\ : std_logic;
SIGNAL \fast|cnt[15]~57\ : std_logic;
SIGNAL \fast|cnt[16]~58_combout\ : std_logic;
SIGNAL \fast|cnt[16]~59\ : std_logic;
SIGNAL \fast|cnt[17]~60_combout\ : std_logic;
SIGNAL \fast|cnt[17]~61\ : std_logic;
SIGNAL \fast|cnt[18]~62_combout\ : std_logic;
SIGNAL \fast|cnt[18]~63\ : std_logic;
SIGNAL \fast|cnt[19]~65\ : std_logic;
SIGNAL \fast|cnt[20]~66_combout\ : std_logic;
SIGNAL \fast|cnt[20]~67\ : std_logic;
SIGNAL \fast|cnt[21]~69\ : std_logic;
SIGNAL \fast|cnt[22]~70_combout\ : std_logic;
SIGNAL \fast|cnt[22]~71\ : std_logic;
SIGNAL \fast|cnt[23]~72_combout\ : std_logic;
SIGNAL \fast|cnt[23]~73\ : std_logic;
SIGNAL \fast|cnt[24]~74_combout\ : std_logic;
SIGNAL \fast|LessThan0~6_combout\ : std_logic;
SIGNAL \fast|LessThan0~7_combout\ : std_logic;
SIGNAL \fast|cnt[21]~68_combout\ : std_logic;
SIGNAL \fast|LessThan0~8_combout\ : std_logic;
SIGNAL \fast|LessThan0~9_combout\ : std_logic;
SIGNAL \fast|LessThan0~10_combout\ : std_logic;
SIGNAL \fast|cnt[1]~29\ : std_logic;
SIGNAL \fast|cnt[2]~31\ : std_logic;
SIGNAL \fast|cnt[3]~33\ : std_logic;
SIGNAL \fast|cnt[4]~34_combout\ : std_logic;
SIGNAL \fast|cnt[4]~35\ : std_logic;
SIGNAL \fast|cnt[5]~37\ : std_logic;
SIGNAL \fast|cnt[6]~38_combout\ : std_logic;
SIGNAL \fast|cnt[6]~39\ : std_logic;
SIGNAL \fast|cnt[7]~41\ : std_logic;
SIGNAL \fast|cnt[8]~42_combout\ : std_logic;
SIGNAL \fast|LessThan0~1_combout\ : std_logic;
SIGNAL \fast|cnt[5]~36_combout\ : std_logic;
SIGNAL \fast|LessThan0~3_combout\ : std_logic;
SIGNAL \fast|cnt[7]~40_combout\ : std_logic;
SIGNAL \fast|LessThan0~0_combout\ : std_logic;
SIGNAL \fast|LessThan0~4_combout\ : std_logic;
SIGNAL \fast|yi~0_combout\ : std_logic;
SIGNAL \fast|yi~regout\ : std_logic;
SIGNAL \pl|altpll_component|_locked\ : std_logic;
SIGNAL \fast|cnt\ : std_logic_vector(25 DOWNTO 0);
SIGNAL \normal|cnt\ : std_logic_vector(25 DOWNTO 0);
SIGNAL \ALT_INV_rstn~combout\ : std_logic;

BEGIN

L1 <= ww_L1;
L2 <= ww_L2;
locked <= ww_locked;
ww_clk <= clk;
ww_rstn <= rstn;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\pl|altpll_component|pll_INCLK_bus\ <= (gnd & \clk~combout\);

\pl|altpll_component|_clk0\ <= \pl|altpll_component|pll_CLK_bus\(0);
\pl|altpll_component|pll~CLK1\ <= \pl|altpll_component|pll_CLK_bus\(1);
\pl|altpll_component|pll~CLK2\ <= \pl|altpll_component|pll_CLK_bus\(2);

\pl|altpll_component|_clk0~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \pl|altpll_component|_clk0\);

\clk~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \clk~combout\);
\ALT_INV_rstn~combout\ <= NOT \rstn~combout\;

-- Location: LCFF_X48_Y8_N11
\normal|cnt[18]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[18]~62_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(18));

-- Location: LCFF_X48_Y9_N7
\normal|cnt[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[0]~26_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(0));

-- Location: LCFF_X48_Y9_N11
\normal|cnt[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[2]~30_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(2));

-- Location: LCFF_X48_Y9_N13
\normal|cnt[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[3]~32_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(3));

-- Location: LCFF_X48_Y8_N13
\normal|cnt[19]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[19]~64_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(19));

-- Location: LCFF_X48_Y8_N21
\normal|cnt[23]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[23]~72_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(23));

-- Location: LCFF_X23_Y4_N25
\fast|cnt[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[9]~44_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(9));

-- Location: LCFF_X23_Y4_N7
\fast|cnt[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[0]~26_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(0));

-- Location: LCFF_X23_Y4_N11
\fast|cnt[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[2]~30_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(2));

-- Location: LCFF_X23_Y4_N13
\fast|cnt[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[3]~32_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(3));

-- Location: LCFF_X22_Y4_N13
\fast|cnt[13]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	sdata => \fast|cnt[13]~52_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(13));

-- Location: LCFF_X22_Y4_N29
\fast|cnt[15]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	sdata => \fast|cnt[15]~56_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(15));

-- Location: LCFF_X23_Y3_N13
\fast|cnt[19]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[19]~64_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(19));

-- Location: LCFF_X23_Y3_N25
\fast|cnt[25]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[25]~76_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(25));

-- Location: LCCOMB_X48_Y9_N6
\normal|cnt[0]~26\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[0]~26_combout\ = \normal|cnt\(0) $ (VCC)
-- \normal|cnt[0]~27\ = CARRY(\normal|cnt\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \normal|cnt\(0),
	datad => VCC,
	combout => \normal|cnt[0]~26_combout\,
	cout => \normal|cnt[0]~27\);

-- Location: LCCOMB_X48_Y9_N10
\normal|cnt[2]~30\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[2]~30_combout\ = (\normal|cnt\(2) & (\normal|cnt[1]~29\ $ (GND))) # (!\normal|cnt\(2) & (!\normal|cnt[1]~29\ & VCC))
-- \normal|cnt[2]~31\ = CARRY((\normal|cnt\(2) & !\normal|cnt[1]~29\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \normal|cnt\(2),
	datad => VCC,
	cin => \normal|cnt[1]~29\,
	combout => \normal|cnt[2]~30_combout\,
	cout => \normal|cnt[2]~31\);

-- Location: LCCOMB_X48_Y9_N12
\normal|cnt[3]~32\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[3]~32_combout\ = (\normal|cnt\(3) & (!\normal|cnt[2]~31\)) # (!\normal|cnt\(3) & ((\normal|cnt[2]~31\) # (GND)))
-- \normal|cnt[3]~33\ = CARRY((!\normal|cnt[2]~31\) # (!\normal|cnt\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \normal|cnt\(3),
	datad => VCC,
	cin => \normal|cnt[2]~31\,
	combout => \normal|cnt[3]~32_combout\,
	cout => \normal|cnt[3]~33\);

-- Location: LCCOMB_X48_Y8_N10
\normal|cnt[18]~62\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[18]~62_combout\ = (\normal|cnt\(18) & (\normal|cnt[17]~61\ $ (GND))) # (!\normal|cnt\(18) & (!\normal|cnt[17]~61\ & VCC))
-- \normal|cnt[18]~63\ = CARRY((\normal|cnt\(18) & !\normal|cnt[17]~61\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \normal|cnt\(18),
	datad => VCC,
	cin => \normal|cnt[17]~61\,
	combout => \normal|cnt[18]~62_combout\,
	cout => \normal|cnt[18]~63\);

-- Location: LCCOMB_X48_Y8_N12
\normal|cnt[19]~64\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[19]~64_combout\ = (\normal|cnt\(19) & (!\normal|cnt[18]~63\)) # (!\normal|cnt\(19) & ((\normal|cnt[18]~63\) # (GND)))
-- \normal|cnt[19]~65\ = CARRY((!\normal|cnt[18]~63\) # (!\normal|cnt\(19)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \normal|cnt\(19),
	datad => VCC,
	cin => \normal|cnt[18]~63\,
	combout => \normal|cnt[19]~64_combout\,
	cout => \normal|cnt[19]~65\);

-- Location: LCCOMB_X48_Y8_N20
\normal|cnt[23]~72\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[23]~72_combout\ = (\normal|cnt\(23) & (!\normal|cnt[22]~71\)) # (!\normal|cnt\(23) & ((\normal|cnt[22]~71\) # (GND)))
-- \normal|cnt[23]~73\ = CARRY((!\normal|cnt[22]~71\) # (!\normal|cnt\(23)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \normal|cnt\(23),
	datad => VCC,
	cin => \normal|cnt[22]~71\,
	combout => \normal|cnt[23]~72_combout\,
	cout => \normal|cnt[23]~73\);

-- Location: LCCOMB_X23_Y4_N6
\fast|cnt[0]~26\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[0]~26_combout\ = \fast|cnt\(0) $ (VCC)
-- \fast|cnt[0]~27\ = CARRY(\fast|cnt\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fast|cnt\(0),
	datad => VCC,
	combout => \fast|cnt[0]~26_combout\,
	cout => \fast|cnt[0]~27\);

-- Location: LCCOMB_X23_Y4_N10
\fast|cnt[2]~30\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[2]~30_combout\ = (\fast|cnt\(2) & (\fast|cnt[1]~29\ $ (GND))) # (!\fast|cnt\(2) & (!\fast|cnt[1]~29\ & VCC))
-- \fast|cnt[2]~31\ = CARRY((\fast|cnt\(2) & !\fast|cnt[1]~29\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fast|cnt\(2),
	datad => VCC,
	cin => \fast|cnt[1]~29\,
	combout => \fast|cnt[2]~30_combout\,
	cout => \fast|cnt[2]~31\);

-- Location: LCCOMB_X23_Y4_N12
\fast|cnt[3]~32\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[3]~32_combout\ = (\fast|cnt\(3) & (!\fast|cnt[2]~31\)) # (!\fast|cnt\(3) & ((\fast|cnt[2]~31\) # (GND)))
-- \fast|cnt[3]~33\ = CARRY((!\fast|cnt[2]~31\) # (!\fast|cnt\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fast|cnt\(3),
	datad => VCC,
	cin => \fast|cnt[2]~31\,
	combout => \fast|cnt[3]~32_combout\,
	cout => \fast|cnt[3]~33\);

-- Location: LCCOMB_X23_Y4_N24
\fast|cnt[9]~44\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[9]~44_combout\ = (\fast|cnt\(9) & (!\fast|cnt[8]~43\)) # (!\fast|cnt\(9) & ((\fast|cnt[8]~43\) # (GND)))
-- \fast|cnt[9]~45\ = CARRY((!\fast|cnt[8]~43\) # (!\fast|cnt\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fast|cnt\(9),
	datad => VCC,
	cin => \fast|cnt[8]~43\,
	combout => \fast|cnt[9]~44_combout\,
	cout => \fast|cnt[9]~45\);

-- Location: LCCOMB_X23_Y3_N0
\fast|cnt[13]~52\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[13]~52_combout\ = (\fast|cnt\(13) & (!\fast|cnt[12]~51\)) # (!\fast|cnt\(13) & ((\fast|cnt[12]~51\) # (GND)))
-- \fast|cnt[13]~53\ = CARRY((!\fast|cnt[12]~51\) # (!\fast|cnt\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fast|cnt\(13),
	datad => VCC,
	cin => \fast|cnt[12]~51\,
	combout => \fast|cnt[13]~52_combout\,
	cout => \fast|cnt[13]~53\);

-- Location: LCCOMB_X23_Y3_N4
\fast|cnt[15]~56\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[15]~56_combout\ = (\fast|cnt\(15) & (!\fast|cnt[14]~55\)) # (!\fast|cnt\(15) & ((\fast|cnt[14]~55\) # (GND)))
-- \fast|cnt[15]~57\ = CARRY((!\fast|cnt[14]~55\) # (!\fast|cnt\(15)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fast|cnt\(15),
	datad => VCC,
	cin => \fast|cnt[14]~55\,
	combout => \fast|cnt[15]~56_combout\,
	cout => \fast|cnt[15]~57\);

-- Location: LCCOMB_X23_Y3_N12
\fast|cnt[19]~64\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[19]~64_combout\ = (\fast|cnt\(19) & (!\fast|cnt[18]~63\)) # (!\fast|cnt\(19) & ((\fast|cnt[18]~63\) # (GND)))
-- \fast|cnt[19]~65\ = CARRY((!\fast|cnt[18]~63\) # (!\fast|cnt\(19)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fast|cnt\(19),
	datad => VCC,
	cin => \fast|cnt[18]~63\,
	combout => \fast|cnt[19]~64_combout\,
	cout => \fast|cnt[19]~65\);

-- Location: LCCOMB_X23_Y3_N22
\fast|cnt[24]~74\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[24]~74_combout\ = (\fast|cnt\(24) & (\fast|cnt[23]~73\ $ (GND))) # (!\fast|cnt\(24) & (!\fast|cnt[23]~73\ & VCC))
-- \fast|cnt[24]~75\ = CARRY((\fast|cnt\(24) & !\fast|cnt[23]~73\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fast|cnt\(24),
	datad => VCC,
	cin => \fast|cnt[23]~73\,
	combout => \fast|cnt[24]~74_combout\,
	cout => \fast|cnt[24]~75\);

-- Location: LCCOMB_X23_Y3_N24
\fast|cnt[25]~76\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[25]~76_combout\ = \fast|cnt\(25) $ (\fast|cnt[24]~75\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fast|cnt\(25),
	cin => \fast|cnt[24]~75\,
	combout => \fast|cnt[25]~76_combout\);

-- Location: LCCOMB_X48_Y9_N2
\normal|LessThan0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|LessThan0~2_combout\ = (((!\normal|cnt\(0)) # (!\normal|cnt\(1))) # (!\normal|cnt\(3))) # (!\normal|cnt\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \normal|cnt\(2),
	datab => \normal|cnt\(3),
	datac => \normal|cnt\(1),
	datad => \normal|cnt\(0),
	combout => \normal|LessThan0~2_combout\);

-- Location: LCCOMB_X48_Y8_N28
\normal|LessThan0~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|LessThan0~7_combout\ = (((!\normal|cnt\(17) & !\normal|cnt\(18))) # (!\normal|cnt\(20))) # (!\normal|cnt\(19))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111101111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \normal|cnt\(19),
	datab => \normal|cnt\(17),
	datac => \normal|cnt\(20),
	datad => \normal|cnt\(18),
	combout => \normal|LessThan0~7_combout\);

-- Location: LCCOMB_X23_Y4_N0
\fast|LessThan0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|LessThan0~2_combout\ = (((!\fast|cnt\(3)) # (!\fast|cnt\(0))) # (!\fast|cnt\(1))) # (!\fast|cnt\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fast|cnt\(2),
	datab => \fast|cnt\(1),
	datac => \fast|cnt\(0),
	datad => \fast|cnt\(3),
	combout => \fast|LessThan0~2_combout\);

-- Location: LCCOMB_X22_Y4_N30
\fast|LessThan0~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|LessThan0~5_combout\ = (((!\fast|cnt\(12)) # (!\fast|cnt\(15))) # (!\fast|cnt\(14))) # (!\fast|cnt\(13))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fast|cnt\(13),
	datab => \fast|cnt\(14),
	datac => \fast|cnt\(15),
	datad => \fast|cnt\(12),
	combout => \fast|LessThan0~5_combout\);

-- Location: PIN_L1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\clk~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_clk,
	combout => \clk~combout\);

-- Location: CLKCTRL_G2
\clk~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~clkctrl_outclk\);

-- Location: LCCOMB_X48_Y9_N8
\normal|cnt[1]~28\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[1]~28_combout\ = (\normal|cnt\(1) & (!\normal|cnt[0]~27\)) # (!\normal|cnt\(1) & ((\normal|cnt[0]~27\) # (GND)))
-- \normal|cnt[1]~29\ = CARRY((!\normal|cnt[0]~27\) # (!\normal|cnt\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \normal|cnt\(1),
	datad => VCC,
	cin => \normal|cnt[0]~27\,
	combout => \normal|cnt[1]~28_combout\,
	cout => \normal|cnt[1]~29\);

-- Location: PIN_R22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\rstn~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_rstn,
	combout => \rstn~combout\);

-- Location: LCCOMB_X48_Y9_N24
\normal|cnt[9]~44\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[9]~44_combout\ = (\normal|cnt\(9) & (!\normal|cnt[8]~43\)) # (!\normal|cnt\(9) & ((\normal|cnt[8]~43\) # (GND)))
-- \normal|cnt[9]~45\ = CARRY((!\normal|cnt[8]~43\) # (!\normal|cnt\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \normal|cnt\(9),
	datad => VCC,
	cin => \normal|cnt[8]~43\,
	combout => \normal|cnt[9]~44_combout\,
	cout => \normal|cnt[9]~45\);

-- Location: LCFF_X48_Y9_N25
\normal|cnt[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[9]~44_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(9));

-- Location: LCCOMB_X48_Y9_N4
\normal|LessThan0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|LessThan0~1_combout\ = (!\normal|cnt\(8) & (!\normal|cnt\(11) & (!\normal|cnt\(9) & !\normal|cnt\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \normal|cnt\(8),
	datab => \normal|cnt\(11),
	datac => \normal|cnt\(9),
	datad => \normal|cnt\(10),
	combout => \normal|LessThan0~1_combout\);

-- Location: LCCOMB_X48_Y9_N16
\normal|cnt[5]~36\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[5]~36_combout\ = (\normal|cnt\(5) & (!\normal|cnt[4]~35\)) # (!\normal|cnt\(5) & ((\normal|cnt[4]~35\) # (GND)))
-- \normal|cnt[5]~37\ = CARRY((!\normal|cnt[4]~35\) # (!\normal|cnt\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \normal|cnt\(5),
	datad => VCC,
	cin => \normal|cnt[4]~35\,
	combout => \normal|cnt[5]~36_combout\,
	cout => \normal|cnt[5]~37\);

-- Location: LCFF_X48_Y9_N17
\normal|cnt[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[5]~36_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(5));

-- Location: LCCOMB_X48_Y9_N0
\normal|LessThan0~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|LessThan0~3_combout\ = ((!\normal|cnt\(6)) # (!\normal|cnt\(5))) # (!\normal|cnt\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \normal|cnt\(4),
	datac => \normal|cnt\(5),
	datad => \normal|cnt\(6),
	combout => \normal|LessThan0~3_combout\);

-- Location: LCCOMB_X48_Y8_N2
\normal|cnt[14]~54\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[14]~54_combout\ = (\normal|cnt\(14) & (\normal|cnt[13]~53\ $ (GND))) # (!\normal|cnt\(14) & (!\normal|cnt[13]~53\ & VCC))
-- \normal|cnt[14]~55\ = CARRY((\normal|cnt\(14) & !\normal|cnt[13]~53\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \normal|cnt\(14),
	datad => VCC,
	cin => \normal|cnt[13]~53\,
	combout => \normal|cnt[14]~54_combout\,
	cout => \normal|cnt[14]~55\);

-- Location: LCCOMB_X48_Y8_N4
\normal|cnt[15]~56\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[15]~56_combout\ = (\normal|cnt\(15) & (!\normal|cnt[14]~55\)) # (!\normal|cnt\(15) & ((\normal|cnt[14]~55\) # (GND)))
-- \normal|cnt[15]~57\ = CARRY((!\normal|cnt[14]~55\) # (!\normal|cnt\(15)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \normal|cnt\(15),
	datad => VCC,
	cin => \normal|cnt[14]~55\,
	combout => \normal|cnt[15]~56_combout\,
	cout => \normal|cnt[15]~57\);

-- Location: LCFF_X48_Y8_N5
\normal|cnt[15]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[15]~56_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(15));

-- Location: LCCOMB_X48_Y8_N6
\normal|cnt[16]~58\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[16]~58_combout\ = (\normal|cnt\(16) & (\normal|cnt[15]~57\ $ (GND))) # (!\normal|cnt\(16) & (!\normal|cnt[15]~57\ & VCC))
-- \normal|cnt[16]~59\ = CARRY((\normal|cnt\(16) & !\normal|cnt[15]~57\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \normal|cnt\(16),
	datad => VCC,
	cin => \normal|cnt[15]~57\,
	combout => \normal|cnt[16]~58_combout\,
	cout => \normal|cnt[16]~59\);

-- Location: LCFF_X48_Y8_N7
\normal|cnt[16]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[16]~58_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(16));

-- Location: LCCOMB_X48_Y9_N20
\normal|cnt[7]~40\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[7]~40_combout\ = (\normal|cnt\(7) & (!\normal|cnt[6]~39\)) # (!\normal|cnt\(7) & ((\normal|cnt[6]~39\) # (GND)))
-- \normal|cnt[7]~41\ = CARRY((!\normal|cnt[6]~39\) # (!\normal|cnt\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \normal|cnt\(7),
	datad => VCC,
	cin => \normal|cnt[6]~39\,
	combout => \normal|cnt[7]~40_combout\,
	cout => \normal|cnt[7]~41\);

-- Location: LCFF_X48_Y9_N21
\normal|cnt[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[7]~40_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(7));

-- Location: LCCOMB_X48_Y8_N8
\normal|cnt[17]~60\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[17]~60_combout\ = (\normal|cnt\(17) & (!\normal|cnt[16]~59\)) # (!\normal|cnt\(17) & ((\normal|cnt[16]~59\) # (GND)))
-- \normal|cnt[17]~61\ = CARRY((!\normal|cnt[16]~59\) # (!\normal|cnt\(17)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \normal|cnt\(17),
	datad => VCC,
	cin => \normal|cnt[16]~59\,
	combout => \normal|cnt[17]~60_combout\,
	cout => \normal|cnt[17]~61\);

-- Location: LCFF_X48_Y8_N9
\normal|cnt[17]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[17]~60_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(17));

-- Location: LCCOMB_X48_Y8_N14
\normal|cnt[20]~66\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[20]~66_combout\ = (\normal|cnt\(20) & (\normal|cnt[19]~65\ $ (GND))) # (!\normal|cnt\(20) & (!\normal|cnt[19]~65\ & VCC))
-- \normal|cnt[20]~67\ = CARRY((\normal|cnt\(20) & !\normal|cnt[19]~65\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \normal|cnt\(20),
	datad => VCC,
	cin => \normal|cnt[19]~65\,
	combout => \normal|cnt[20]~66_combout\,
	cout => \normal|cnt[20]~67\);

-- Location: LCFF_X48_Y8_N15
\normal|cnt[20]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[20]~66_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(20));

-- Location: LCCOMB_X48_Y8_N16
\normal|cnt[21]~68\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[21]~68_combout\ = (\normal|cnt\(21) & (!\normal|cnt[20]~67\)) # (!\normal|cnt\(21) & ((\normal|cnt[20]~67\) # (GND)))
-- \normal|cnt[21]~69\ = CARRY((!\normal|cnt[20]~67\) # (!\normal|cnt\(21)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \normal|cnt\(21),
	datad => VCC,
	cin => \normal|cnt[20]~67\,
	combout => \normal|cnt[21]~68_combout\,
	cout => \normal|cnt[21]~69\);

-- Location: LCCOMB_X48_Y8_N18
\normal|cnt[22]~70\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[22]~70_combout\ = (\normal|cnt\(22) & (\normal|cnt[21]~69\ $ (GND))) # (!\normal|cnt\(22) & (!\normal|cnt[21]~69\ & VCC))
-- \normal|cnt[22]~71\ = CARRY((\normal|cnt\(22) & !\normal|cnt[21]~69\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \normal|cnt\(22),
	datad => VCC,
	cin => \normal|cnt[21]~69\,
	combout => \normal|cnt[22]~70_combout\,
	cout => \normal|cnt[22]~71\);

-- Location: LCFF_X48_Y8_N19
\normal|cnt[22]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[22]~70_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(22));

-- Location: LCCOMB_X48_Y8_N22
\normal|cnt[24]~74\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[24]~74_combout\ = (\normal|cnt\(24) & (\normal|cnt[23]~73\ $ (GND))) # (!\normal|cnt\(24) & (!\normal|cnt[23]~73\ & VCC))
-- \normal|cnt[24]~75\ = CARRY((\normal|cnt\(24) & !\normal|cnt[23]~73\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \normal|cnt\(24),
	datad => VCC,
	cin => \normal|cnt[23]~73\,
	combout => \normal|cnt[24]~74_combout\,
	cout => \normal|cnt[24]~75\);

-- Location: LCFF_X48_Y8_N23
\normal|cnt[24]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[24]~74_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(24));

-- Location: LCCOMB_X49_Y8_N10
\normal|LessThan0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|LessThan0~0_combout\ = (!\normal|cnt\(18) & (!\normal|cnt\(16) & (!\normal|cnt\(7) & !\normal|cnt\(24))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \normal|cnt\(18),
	datab => \normal|cnt\(16),
	datac => \normal|cnt\(7),
	datad => \normal|cnt\(24),
	combout => \normal|LessThan0~0_combout\);

-- Location: LCCOMB_X49_Y9_N16
\normal|LessThan0~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|LessThan0~4_combout\ = (\normal|LessThan0~1_combout\ & (\normal|LessThan0~0_combout\ & ((\normal|LessThan0~2_combout\) # (\normal|LessThan0~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \normal|LessThan0~2_combout\,
	datab => \normal|LessThan0~1_combout\,
	datac => \normal|LessThan0~3_combout\,
	datad => \normal|LessThan0~0_combout\,
	combout => \normal|LessThan0~4_combout\);

-- Location: LCCOMB_X48_Y8_N24
\normal|cnt[25]~76\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[25]~76_combout\ = \normal|cnt\(25) $ (\normal|cnt[24]~75\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \normal|cnt\(25),
	cin => \normal|cnt[24]~75\,
	combout => \normal|cnt[25]~76_combout\);

-- Location: LCFF_X48_Y8_N25
\normal|cnt[25]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[25]~76_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(25));

-- Location: LCFF_X48_Y8_N17
\normal|cnt[21]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[21]~68_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(21));

-- Location: LCCOMB_X48_Y8_N30
\normal|LessThan0~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|LessThan0~8_combout\ = ((!\normal|cnt\(22)) # (!\normal|cnt\(21))) # (!\normal|cnt\(23))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \normal|cnt\(23),
	datac => \normal|cnt\(21),
	datad => \normal|cnt\(22),
	combout => \normal|LessThan0~8_combout\);

-- Location: LCCOMB_X49_Y8_N2
\normal|LessThan0~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|LessThan0~9_combout\ = ((!\normal|cnt\(24) & ((\normal|LessThan0~7_combout\) # (\normal|LessThan0~8_combout\)))) # (!\normal|cnt\(25))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \normal|LessThan0~7_combout\,
	datab => \normal|cnt\(25),
	datac => \normal|LessThan0~8_combout\,
	datad => \normal|cnt\(24),
	combout => \normal|LessThan0~9_combout\);

-- Location: LCCOMB_X49_Y8_N0
\normal|LessThan0~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|LessThan0~10_combout\ = (!\normal|LessThan0~6_combout\ & (!\normal|LessThan0~4_combout\ & !\normal|LessThan0~9_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \normal|LessThan0~6_combout\,
	datac => \normal|LessThan0~4_combout\,
	datad => \normal|LessThan0~9_combout\,
	combout => \normal|LessThan0~10_combout\);

-- Location: LCFF_X48_Y9_N9
\normal|cnt[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[1]~28_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(1));

-- Location: LCCOMB_X48_Y9_N14
\normal|cnt[4]~34\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[4]~34_combout\ = (\normal|cnt\(4) & (\normal|cnt[3]~33\ $ (GND))) # (!\normal|cnt\(4) & (!\normal|cnt[3]~33\ & VCC))
-- \normal|cnt[4]~35\ = CARRY((\normal|cnt\(4) & !\normal|cnt[3]~33\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \normal|cnt\(4),
	datad => VCC,
	cin => \normal|cnt[3]~33\,
	combout => \normal|cnt[4]~34_combout\,
	cout => \normal|cnt[4]~35\);

-- Location: LCFF_X48_Y9_N15
\normal|cnt[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[4]~34_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(4));

-- Location: LCCOMB_X48_Y9_N18
\normal|cnt[6]~38\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[6]~38_combout\ = (\normal|cnt\(6) & (\normal|cnt[5]~37\ $ (GND))) # (!\normal|cnt\(6) & (!\normal|cnt[5]~37\ & VCC))
-- \normal|cnt[6]~39\ = CARRY((\normal|cnt\(6) & !\normal|cnt[5]~37\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \normal|cnt\(6),
	datad => VCC,
	cin => \normal|cnt[5]~37\,
	combout => \normal|cnt[6]~38_combout\,
	cout => \normal|cnt[6]~39\);

-- Location: LCFF_X48_Y9_N19
\normal|cnt[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[6]~38_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(6));

-- Location: LCCOMB_X48_Y9_N22
\normal|cnt[8]~42\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[8]~42_combout\ = (\normal|cnt\(8) & (\normal|cnt[7]~41\ $ (GND))) # (!\normal|cnt\(8) & (!\normal|cnt[7]~41\ & VCC))
-- \normal|cnt[8]~43\ = CARRY((\normal|cnt\(8) & !\normal|cnt[7]~41\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \normal|cnt\(8),
	datad => VCC,
	cin => \normal|cnt[7]~41\,
	combout => \normal|cnt[8]~42_combout\,
	cout => \normal|cnt[8]~43\);

-- Location: LCFF_X48_Y9_N23
\normal|cnt[8]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[8]~42_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(8));

-- Location: LCCOMB_X48_Y9_N26
\normal|cnt[10]~46\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[10]~46_combout\ = (\normal|cnt\(10) & (\normal|cnt[9]~45\ $ (GND))) # (!\normal|cnt\(10) & (!\normal|cnt[9]~45\ & VCC))
-- \normal|cnt[10]~47\ = CARRY((\normal|cnt\(10) & !\normal|cnt[9]~45\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \normal|cnt\(10),
	datad => VCC,
	cin => \normal|cnt[9]~45\,
	combout => \normal|cnt[10]~46_combout\,
	cout => \normal|cnt[10]~47\);

-- Location: LCFF_X48_Y9_N27
\normal|cnt[10]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[10]~46_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(10));

-- Location: LCCOMB_X48_Y9_N28
\normal|cnt[11]~48\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[11]~48_combout\ = (\normal|cnt\(11) & (!\normal|cnt[10]~47\)) # (!\normal|cnt\(11) & ((\normal|cnt[10]~47\) # (GND)))
-- \normal|cnt[11]~49\ = CARRY((!\normal|cnt[10]~47\) # (!\normal|cnt\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \normal|cnt\(11),
	datad => VCC,
	cin => \normal|cnt[10]~47\,
	combout => \normal|cnt[11]~48_combout\,
	cout => \normal|cnt[11]~49\);

-- Location: LCFF_X48_Y9_N29
\normal|cnt[11]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[11]~48_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(11));

-- Location: LCCOMB_X48_Y9_N30
\normal|cnt[12]~50\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[12]~50_combout\ = (\normal|cnt\(12) & (\normal|cnt[11]~49\ $ (GND))) # (!\normal|cnt\(12) & (!\normal|cnt[11]~49\ & VCC))
-- \normal|cnt[12]~51\ = CARRY((\normal|cnt\(12) & !\normal|cnt[11]~49\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \normal|cnt\(12),
	datad => VCC,
	cin => \normal|cnt[11]~49\,
	combout => \normal|cnt[12]~50_combout\,
	cout => \normal|cnt[12]~51\);

-- Location: LCFF_X48_Y9_N31
\normal|cnt[12]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[12]~50_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(12));

-- Location: LCCOMB_X48_Y8_N0
\normal|cnt[13]~52\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|cnt[13]~52_combout\ = (\normal|cnt\(13) & (!\normal|cnt[12]~51\)) # (!\normal|cnt\(13) & ((\normal|cnt[12]~51\) # (GND)))
-- \normal|cnt[13]~53\ = CARRY((!\normal|cnt[12]~51\) # (!\normal|cnt\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \normal|cnt\(13),
	datad => VCC,
	cin => \normal|cnt[12]~51\,
	combout => \normal|cnt[13]~52_combout\,
	cout => \normal|cnt[13]~53\);

-- Location: LCFF_X48_Y8_N1
\normal|cnt[13]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[13]~52_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(13));

-- Location: LCFF_X48_Y8_N3
\normal|cnt[14]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|cnt[14]~54_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \normal|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|cnt\(14));

-- Location: LCCOMB_X48_Y8_N26
\normal|LessThan0~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|LessThan0~5_combout\ = (((!\normal|cnt\(13)) # (!\normal|cnt\(15))) # (!\normal|cnt\(14))) # (!\normal|cnt\(12))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \normal|cnt\(12),
	datab => \normal|cnt\(14),
	datac => \normal|cnt\(15),
	datad => \normal|cnt\(13),
	combout => \normal|LessThan0~5_combout\);

-- Location: LCCOMB_X49_Y8_N28
\normal|LessThan0~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|LessThan0~6_combout\ = (!\normal|cnt\(18) & (\normal|LessThan0~5_combout\ & (!\normal|cnt\(16) & !\normal|cnt\(24))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \normal|cnt\(18),
	datab => \normal|LessThan0~5_combout\,
	datac => \normal|cnt\(16),
	datad => \normal|cnt\(24),
	combout => \normal|LessThan0~6_combout\);

-- Location: LCCOMB_X49_Y8_N16
\normal|yi~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \normal|yi~0_combout\ = \normal|yi~regout\ $ (((!\normal|LessThan0~4_combout\ & (!\normal|LessThan0~6_combout\ & !\normal|LessThan0~9_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \normal|LessThan0~4_combout\,
	datab => \normal|LessThan0~6_combout\,
	datac => \normal|yi~regout\,
	datad => \normal|LessThan0~9_combout\,
	combout => \normal|yi~0_combout\);

-- Location: LCFF_X49_Y8_N17
\normal|yi\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \normal|yi~0_combout\,
	aclr => \ALT_INV_rstn~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \normal|yi~regout\);

-- Location: PLL_1
\pl|altpll_component|pll\ : cycloneii_pll
-- pragma translate_off
GENERIC MAP (
	bandwidth => 0,
	bandwidth_type => "low",
	c0_high => 2,
	c0_initial => 1,
	c0_low => 2,
	c0_mode => "even",
	c0_ph => 0,
	c1_mode => "bypass",
	c1_ph => 0,
	c2_mode => "bypass",
	c2_ph => 0,
	charge_pump_current => 80,
	clk0_counter => "c0",
	clk0_divide_by => 1,
	clk0_duty_cycle => 50,
	clk0_multiply_by => 4,
	clk0_phase_shift => "0",
	clk1_duty_cycle => 50,
	clk1_phase_shift => "0",
	clk2_duty_cycle => 50,
	clk2_phase_shift => "0",
	compensate_clock => "clk0",
	gate_lock_counter => 0,
	gate_lock_signal => "no",
	inclk0_input_frequency => 20000,
	inclk1_input_frequency => 20000,
	invalid_lock_multiplier => 5,
	loop_filter_c => 3,
	loop_filter_r => " 2.500000",
	m => 16,
	m_initial => 1,
	m_ph => 0,
	n => 1,
	operation_mode => "normal",
	pfd_max => 100000,
	pfd_min => 2484,
	pll_compensation_delay => 5937,
	self_reset_on_gated_loss_lock => "off",
	sim_gate_lock_device_behavior => "off",
	simulation_type => "timing",
	valid_lock_multiplier => 1,
	vco_center => 1333,
	vco_max => 2000,
	vco_min => 1000)
-- pragma translate_on
PORT MAP (
	areset => \ALT_INV_rstn~combout\,
	inclk => \pl|altpll_component|pll_INCLK_bus\,
	locked => \pl|altpll_component|_locked\,
	clk => \pl|altpll_component|pll_CLK_bus\);

-- Location: CLKCTRL_G3
\pl|altpll_component|_clk0~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \pl|altpll_component|_clk0~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \pl|altpll_component|_clk0~clkctrl_outclk\);

-- Location: LCCOMB_X23_Y4_N8
\fast|cnt[1]~28\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[1]~28_combout\ = (\fast|cnt\(1) & (!\fast|cnt[0]~27\)) # (!\fast|cnt\(1) & ((\fast|cnt[0]~27\) # (GND)))
-- \fast|cnt[1]~29\ = CARRY((!\fast|cnt[0]~27\) # (!\fast|cnt\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fast|cnt\(1),
	datad => VCC,
	cin => \fast|cnt[0]~27\,
	combout => \fast|cnt[1]~28_combout\,
	cout => \fast|cnt[1]~29\);

-- Location: LCCOMB_X23_Y4_N22
\fast|cnt[8]~42\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[8]~42_combout\ = (\fast|cnt\(8) & (\fast|cnt[7]~41\ $ (GND))) # (!\fast|cnt\(8) & (!\fast|cnt[7]~41\ & VCC))
-- \fast|cnt[8]~43\ = CARRY((\fast|cnt\(8) & !\fast|cnt[7]~41\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fast|cnt\(8),
	datad => VCC,
	cin => \fast|cnt[7]~41\,
	combout => \fast|cnt[8]~42_combout\,
	cout => \fast|cnt[8]~43\);

-- Location: LCCOMB_X23_Y4_N26
\fast|cnt[10]~46\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[10]~46_combout\ = (\fast|cnt\(10) & (\fast|cnt[9]~45\ $ (GND))) # (!\fast|cnt\(10) & (!\fast|cnt[9]~45\ & VCC))
-- \fast|cnt[10]~47\ = CARRY((\fast|cnt\(10) & !\fast|cnt[9]~45\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fast|cnt\(10),
	datad => VCC,
	cin => \fast|cnt[9]~45\,
	combout => \fast|cnt[10]~46_combout\,
	cout => \fast|cnt[10]~47\);

-- Location: LCFF_X23_Y4_N27
\fast|cnt[10]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[10]~46_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(10));

-- Location: LCCOMB_X23_Y4_N28
\fast|cnt[11]~48\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[11]~48_combout\ = (\fast|cnt\(11) & (!\fast|cnt[10]~47\)) # (!\fast|cnt\(11) & ((\fast|cnt[10]~47\) # (GND)))
-- \fast|cnt[11]~49\ = CARRY((!\fast|cnt[10]~47\) # (!\fast|cnt\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fast|cnt\(11),
	datad => VCC,
	cin => \fast|cnt[10]~47\,
	combout => \fast|cnt[11]~48_combout\,
	cout => \fast|cnt[11]~49\);

-- Location: LCFF_X23_Y4_N29
\fast|cnt[11]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[11]~48_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(11));

-- Location: LCCOMB_X23_Y4_N30
\fast|cnt[12]~50\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[12]~50_combout\ = (\fast|cnt\(12) & (\fast|cnt[11]~49\ $ (GND))) # (!\fast|cnt\(12) & (!\fast|cnt[11]~49\ & VCC))
-- \fast|cnt[12]~51\ = CARRY((\fast|cnt\(12) & !\fast|cnt[11]~49\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fast|cnt\(12),
	datad => VCC,
	cin => \fast|cnt[11]~49\,
	combout => \fast|cnt[12]~50_combout\,
	cout => \fast|cnt[12]~51\);

-- Location: LCFF_X23_Y4_N31
\fast|cnt[12]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[12]~50_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(12));

-- Location: LCCOMB_X23_Y3_N2
\fast|cnt[14]~54\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[14]~54_combout\ = (\fast|cnt\(14) & (\fast|cnt[13]~53\ $ (GND))) # (!\fast|cnt\(14) & (!\fast|cnt[13]~53\ & VCC))
-- \fast|cnt[14]~55\ = CARRY((\fast|cnt\(14) & !\fast|cnt[13]~53\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fast|cnt\(14),
	datad => VCC,
	cin => \fast|cnt[13]~53\,
	combout => \fast|cnt[14]~54_combout\,
	cout => \fast|cnt[14]~55\);

-- Location: LCFF_X22_Y4_N19
\fast|cnt[14]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	sdata => \fast|cnt[14]~54_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(14));

-- Location: LCCOMB_X23_Y3_N6
\fast|cnt[16]~58\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[16]~58_combout\ = (\fast|cnt\(16) & (\fast|cnt[15]~57\ $ (GND))) # (!\fast|cnt\(16) & (!\fast|cnt[15]~57\ & VCC))
-- \fast|cnt[16]~59\ = CARRY((\fast|cnt\(16) & !\fast|cnt[15]~57\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fast|cnt\(16),
	datad => VCC,
	cin => \fast|cnt[15]~57\,
	combout => \fast|cnt[16]~58_combout\,
	cout => \fast|cnt[16]~59\);

-- Location: LCFF_X23_Y3_N7
\fast|cnt[16]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[16]~58_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(16));

-- Location: LCCOMB_X23_Y3_N8
\fast|cnt[17]~60\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[17]~60_combout\ = (\fast|cnt\(17) & (!\fast|cnt[16]~59\)) # (!\fast|cnt\(17) & ((\fast|cnt[16]~59\) # (GND)))
-- \fast|cnt[17]~61\ = CARRY((!\fast|cnt[16]~59\) # (!\fast|cnt\(17)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fast|cnt\(17),
	datad => VCC,
	cin => \fast|cnt[16]~59\,
	combout => \fast|cnt[17]~60_combout\,
	cout => \fast|cnt[17]~61\);

-- Location: LCFF_X23_Y3_N9
\fast|cnt[17]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[17]~60_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(17));

-- Location: LCCOMB_X23_Y3_N10
\fast|cnt[18]~62\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[18]~62_combout\ = (\fast|cnt\(18) & (\fast|cnt[17]~61\ $ (GND))) # (!\fast|cnt\(18) & (!\fast|cnt[17]~61\ & VCC))
-- \fast|cnt[18]~63\ = CARRY((\fast|cnt\(18) & !\fast|cnt[17]~61\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fast|cnt\(18),
	datad => VCC,
	cin => \fast|cnt[17]~61\,
	combout => \fast|cnt[18]~62_combout\,
	cout => \fast|cnt[18]~63\);

-- Location: LCFF_X23_Y3_N11
\fast|cnt[18]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[18]~62_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(18));

-- Location: LCCOMB_X23_Y3_N14
\fast|cnt[20]~66\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[20]~66_combout\ = (\fast|cnt\(20) & (\fast|cnt[19]~65\ $ (GND))) # (!\fast|cnt\(20) & (!\fast|cnt[19]~65\ & VCC))
-- \fast|cnt[20]~67\ = CARRY((\fast|cnt\(20) & !\fast|cnt[19]~65\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fast|cnt\(20),
	datad => VCC,
	cin => \fast|cnt[19]~65\,
	combout => \fast|cnt[20]~66_combout\,
	cout => \fast|cnt[20]~67\);

-- Location: LCFF_X23_Y3_N15
\fast|cnt[20]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[20]~66_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(20));

-- Location: LCCOMB_X23_Y3_N16
\fast|cnt[21]~68\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[21]~68_combout\ = (\fast|cnt\(21) & (!\fast|cnt[20]~67\)) # (!\fast|cnt\(21) & ((\fast|cnt[20]~67\) # (GND)))
-- \fast|cnt[21]~69\ = CARRY((!\fast|cnt[20]~67\) # (!\fast|cnt\(21)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fast|cnt\(21),
	datad => VCC,
	cin => \fast|cnt[20]~67\,
	combout => \fast|cnt[21]~68_combout\,
	cout => \fast|cnt[21]~69\);

-- Location: LCCOMB_X23_Y3_N18
\fast|cnt[22]~70\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[22]~70_combout\ = (\fast|cnt\(22) & (\fast|cnt[21]~69\ $ (GND))) # (!\fast|cnt\(22) & (!\fast|cnt[21]~69\ & VCC))
-- \fast|cnt[22]~71\ = CARRY((\fast|cnt\(22) & !\fast|cnt[21]~69\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fast|cnt\(22),
	datad => VCC,
	cin => \fast|cnt[21]~69\,
	combout => \fast|cnt[22]~70_combout\,
	cout => \fast|cnt[22]~71\);

-- Location: LCFF_X23_Y3_N19
\fast|cnt[22]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[22]~70_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(22));

-- Location: LCCOMB_X23_Y3_N20
\fast|cnt[23]~72\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[23]~72_combout\ = (\fast|cnt\(23) & (!\fast|cnt[22]~71\)) # (!\fast|cnt\(23) & ((\fast|cnt[22]~71\) # (GND)))
-- \fast|cnt[23]~73\ = CARRY((!\fast|cnt[22]~71\) # (!\fast|cnt\(23)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fast|cnt\(23),
	datad => VCC,
	cin => \fast|cnt[22]~71\,
	combout => \fast|cnt[23]~72_combout\,
	cout => \fast|cnt[23]~73\);

-- Location: LCFF_X23_Y3_N21
\fast|cnt[23]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[23]~72_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(23));

-- Location: LCFF_X23_Y3_N23
\fast|cnt[24]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[24]~74_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(24));

-- Location: LCCOMB_X23_Y4_N2
\fast|LessThan0~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|LessThan0~6_combout\ = (\fast|LessThan0~5_combout\ & (!\fast|cnt\(16) & (!\fast|cnt\(18) & !\fast|cnt\(24))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fast|LessThan0~5_combout\,
	datab => \fast|cnt\(16),
	datac => \fast|cnt\(18),
	datad => \fast|cnt\(24),
	combout => \fast|LessThan0~6_combout\);

-- Location: LCCOMB_X23_Y3_N30
\fast|LessThan0~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|LessThan0~7_combout\ = (((!\fast|cnt\(17) & !\fast|cnt\(18))) # (!\fast|cnt\(20))) # (!\fast|cnt\(19))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011101111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fast|cnt\(19),
	datab => \fast|cnt\(20),
	datac => \fast|cnt\(17),
	datad => \fast|cnt\(18),
	combout => \fast|LessThan0~7_combout\);

-- Location: LCFF_X23_Y3_N17
\fast|cnt[21]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[21]~68_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(21));

-- Location: LCCOMB_X23_Y3_N28
\fast|LessThan0~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|LessThan0~8_combout\ = ((!\fast|cnt\(22)) # (!\fast|cnt\(21))) # (!\fast|cnt\(23))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fast|cnt\(23),
	datac => \fast|cnt\(21),
	datad => \fast|cnt\(22),
	combout => \fast|LessThan0~8_combout\);

-- Location: LCCOMB_X23_Y3_N26
\fast|LessThan0~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|LessThan0~9_combout\ = ((!\fast|cnt\(24) & ((\fast|LessThan0~7_combout\) # (\fast|LessThan0~8_combout\)))) # (!\fast|cnt\(25))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011101110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fast|cnt\(25),
	datab => \fast|cnt\(24),
	datac => \fast|LessThan0~7_combout\,
	datad => \fast|LessThan0~8_combout\,
	combout => \fast|LessThan0~9_combout\);

-- Location: LCCOMB_X23_Y4_N4
\fast|LessThan0~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|LessThan0~10_combout\ = (!\fast|LessThan0~6_combout\ & (!\fast|LessThan0~9_combout\ & !\fast|LessThan0~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \fast|LessThan0~6_combout\,
	datac => \fast|LessThan0~9_combout\,
	datad => \fast|LessThan0~4_combout\,
	combout => \fast|LessThan0~10_combout\);

-- Location: LCFF_X23_Y4_N9
\fast|cnt[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[1]~28_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(1));

-- Location: LCCOMB_X23_Y4_N14
\fast|cnt[4]~34\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[4]~34_combout\ = (\fast|cnt\(4) & (\fast|cnt[3]~33\ $ (GND))) # (!\fast|cnt\(4) & (!\fast|cnt[3]~33\ & VCC))
-- \fast|cnt[4]~35\ = CARRY((\fast|cnt\(4) & !\fast|cnt[3]~33\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fast|cnt\(4),
	datad => VCC,
	cin => \fast|cnt[3]~33\,
	combout => \fast|cnt[4]~34_combout\,
	cout => \fast|cnt[4]~35\);

-- Location: LCFF_X23_Y4_N15
\fast|cnt[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[4]~34_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(4));

-- Location: LCCOMB_X23_Y4_N16
\fast|cnt[5]~36\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[5]~36_combout\ = (\fast|cnt\(5) & (!\fast|cnt[4]~35\)) # (!\fast|cnt\(5) & ((\fast|cnt[4]~35\) # (GND)))
-- \fast|cnt[5]~37\ = CARRY((!\fast|cnt[4]~35\) # (!\fast|cnt\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fast|cnt\(5),
	datad => VCC,
	cin => \fast|cnt[4]~35\,
	combout => \fast|cnt[5]~36_combout\,
	cout => \fast|cnt[5]~37\);

-- Location: LCCOMB_X23_Y4_N18
\fast|cnt[6]~38\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[6]~38_combout\ = (\fast|cnt\(6) & (\fast|cnt[5]~37\ $ (GND))) # (!\fast|cnt\(6) & (!\fast|cnt[5]~37\ & VCC))
-- \fast|cnt[6]~39\ = CARRY((\fast|cnt\(6) & !\fast|cnt[5]~37\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fast|cnt\(6),
	datad => VCC,
	cin => \fast|cnt[5]~37\,
	combout => \fast|cnt[6]~38_combout\,
	cout => \fast|cnt[6]~39\);

-- Location: LCFF_X23_Y4_N19
\fast|cnt[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[6]~38_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(6));

-- Location: LCCOMB_X23_Y4_N20
\fast|cnt[7]~40\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|cnt[7]~40_combout\ = (\fast|cnt\(7) & (!\fast|cnt[6]~39\)) # (!\fast|cnt\(7) & ((\fast|cnt[6]~39\) # (GND)))
-- \fast|cnt[7]~41\ = CARRY((!\fast|cnt[6]~39\) # (!\fast|cnt\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fast|cnt\(7),
	datad => VCC,
	cin => \fast|cnt[6]~39\,
	combout => \fast|cnt[7]~40_combout\,
	cout => \fast|cnt[7]~41\);

-- Location: LCFF_X23_Y4_N23
\fast|cnt[8]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[8]~42_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(8));

-- Location: LCCOMB_X24_Y4_N0
\fast|LessThan0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|LessThan0~1_combout\ = (!\fast|cnt\(9) & (!\fast|cnt\(8) & (!\fast|cnt\(10) & !\fast|cnt\(11))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fast|cnt\(9),
	datab => \fast|cnt\(8),
	datac => \fast|cnt\(10),
	datad => \fast|cnt\(11),
	combout => \fast|LessThan0~1_combout\);

-- Location: LCFF_X23_Y4_N17
\fast|cnt[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[5]~36_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(5));

-- Location: LCCOMB_X24_Y4_N14
\fast|LessThan0~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|LessThan0~3_combout\ = ((!\fast|cnt\(5)) # (!\fast|cnt\(6))) # (!\fast|cnt\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fast|cnt\(4),
	datab => \fast|cnt\(6),
	datad => \fast|cnt\(5),
	combout => \fast|LessThan0~3_combout\);

-- Location: LCFF_X23_Y4_N21
\fast|cnt[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|cnt[7]~40_combout\,
	aclr => \ALT_INV_rstn~combout\,
	sclr => \fast|LessThan0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|cnt\(7));

-- Location: LCCOMB_X24_Y4_N18
\fast|LessThan0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|LessThan0~0_combout\ = (!\fast|cnt\(16) & (!\fast|cnt\(24) & (!\fast|cnt\(7) & !\fast|cnt\(18))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fast|cnt\(16),
	datab => \fast|cnt\(24),
	datac => \fast|cnt\(7),
	datad => \fast|cnt\(18),
	combout => \fast|LessThan0~0_combout\);

-- Location: LCCOMB_X24_Y4_N24
\fast|LessThan0~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|LessThan0~4_combout\ = (\fast|LessThan0~1_combout\ & (\fast|LessThan0~0_combout\ & ((\fast|LessThan0~2_combout\) # (\fast|LessThan0~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fast|LessThan0~2_combout\,
	datab => \fast|LessThan0~1_combout\,
	datac => \fast|LessThan0~3_combout\,
	datad => \fast|LessThan0~0_combout\,
	combout => \fast|LessThan0~4_combout\);

-- Location: LCCOMB_X24_Y4_N20
\fast|yi~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \fast|yi~0_combout\ = \fast|yi~regout\ $ (((!\fast|LessThan0~9_combout\ & (!\fast|LessThan0~4_combout\ & !\fast|LessThan0~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fast|LessThan0~9_combout\,
	datab => \fast|LessThan0~4_combout\,
	datac => \fast|yi~regout\,
	datad => \fast|LessThan0~6_combout\,
	combout => \fast|yi~0_combout\);

-- Location: LCFF_X24_Y4_N21
\fast|yi\ : cycloneii_lcell_ff
PORT MAP (
	clk => \pl|altpll_component|_clk0~clkctrl_outclk\,
	datain => \fast|yi~0_combout\,
	aclr => \ALT_INV_rstn~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fast|yi~regout\);

-- Location: PIN_R19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\L1~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \normal|yi~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_L1);

-- Location: PIN_U19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\L2~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \fast|yi~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_L2);

-- Location: PIN_R20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\locked~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \pl|altpll_component|_locked\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_locked);
END structure;


