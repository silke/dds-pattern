vcom *.vhd
vsim work.testbench
add wave clk rstn L1 L2 locked
add wave {sim:/testbench/d/\pl|altpll_component|_clk0~clkctrl\/outclk}
run 200ns
